﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Policy;
using MySql.Data.MySqlClient;


namespace WcfTrackingService
{
    internal class DatabaseAccess
    {
        private MySqlConnection _connection;
        private string          _connectionString;
        private string          _database;
        private string          _password;
        private string          _server;
        private string          _uid;

        public DatabaseAccess()
        {
            InitializeConnection();
        }

        private void InitializeConnection()
        {
            _server     = "localhost";
            _database   = "ds_assign4_tracking";
            _uid        = "root";
            _password   = "admin";

            _connectionString = "SERVER="   + _server   + ";" + 
                                "DATABASE=" + _database + ";" +
                                "UID="      + _uid      + ";" + 
                                "PASSWORD=" + _password + ";";

            _connection = new MySqlConnection(_connectionString);
        }

        private bool OpenConnection()
        {
            try
            {
                _connection.Open();
                return true;
            }
            catch (MySqlException ex)
            {                
                return false;
            }
        }

        private void CloseConnection()
        {
            _connection.Close();                         
        }

        public bool IsValidUser(string username, string password)
        {            
            const string query = "SELECT admin.username FROM admin WHERE username = @user AND password = @passwd";
       
            var status = false;

            if (OpenConnection())
            {
                var cmd = new MySqlCommand(query, _connection);
                cmd.Parameters.AddWithValue("@user", username);
                cmd.Parameters.AddWithValue("@passwd", password);               
                var dataReader = cmd.ExecuteReader();

                var count = 0;

                while (dataReader.Read())
                {
                    count = count + 1;
                }

                if (count == 1)
                {
                    status = true; // If one of the user exists -> OK
                }

                dataReader.Close();
                CloseConnection();
            }

            return status;
        }

        public List<Package> ReturnAllPackages()
        {
            const string query = "SELECT * FROM package";
            var packages = new List<Package>();

            try
            {
                OpenConnection();

                var cmd = new MySqlCommand(query, _connection);
                var dataReader = cmd.ExecuteReader();

                while(dataReader.Read())
                {
                    var package = new Package
                    {
                        Id           = Convert.ToInt32(dataReader["Id"]),
                        SenderId     = Convert.ToInt32(dataReader["SenderId"]),
                        ReceiverId   = Convert.ToInt32(dataReader["ReceiverId"]),
                        Name         = Convert.ToString(dataReader["Name"]),
                        Description  = Convert.ToString(dataReader["Description"]),
                        SenderCity   = Convert.ToString(dataReader["SenderCity"]),
                        ReceiverCity = Convert.ToString(dataReader["ReceiverCity"]),
                        Tracking     = Convert.ToBoolean(dataReader["Tracking"])
                    };

                    packages.Add(package);                    
                }

                CloseConnection();

                return packages.ToList();
            }
            catch(Exception ex)
            {
                throw ex.InnerException;
            }
        }

        public Package ReturnPackageById(int packageId)
        {
            const string query = "SELECT * FROM package WHERE Id = @id";
            var package = new Package();

            try
            {
                OpenConnection();

                var cmd = new MySqlCommand(query, _connection);
                cmd.Parameters.AddWithValue("@id", packageId);
                var dataReader = cmd.ExecuteReader();
               
                while (dataReader.Read())
                {

                    package.Id = Convert.ToInt32(dataReader["Id"]);
                    package.SenderId = Convert.ToInt32(dataReader["SenderId"]);
                    package.ReceiverId = Convert.ToInt32(dataReader["ReceiverId"]);
                    package.Name = Convert.ToString(dataReader["Name"]);
                    package.Description = Convert.ToString(dataReader["Description"]);
                    package.SenderCity = Convert.ToString(dataReader["SenderCity"]);
                    package.ReceiverCity = Convert.ToString(dataReader["ReceiverCity"]);
                    package.Tracking = Convert.ToBoolean(dataReader["Tracking"]);
                }

                CloseConnection();

                return package;                
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }
        }
       
        public void InsertPackage(Package package)
        {
            const string query = "INSERT INTO package (SenderId, ReceiverId, Name, Description, SenderCity, ReceiverCity, Tracking) " +
                                 "VALUES (@_1, @_2,  @_3 , @_4,  @_5,  @_6,  @_7);";

            try
            {
                OpenConnection();

                var cmd = new MySqlCommand(query, _connection);
                cmd.Parameters.AddWithValue("@_1", package.SenderId);
                cmd.Parameters.AddWithValue("@_2", package.ReceiverId);
                cmd.Parameters.AddWithValue("@_3", package.Name);
                cmd.Parameters.AddWithValue("@_4", package.Description);
                cmd.Parameters.AddWithValue("@_5", package.SenderCity);
                cmd.Parameters.AddWithValue("@_6", package.ReceiverCity);
                cmd.Parameters.AddWithValue("@_7", package.Tracking);
                cmd.ExecuteNonQuery();

                CloseConnection();
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }
        }

        public void DeletePackage(int packageId)
        {
            const string query = "DELETE FROM package WHERE Id = @packageId;";

            try
            {
                OpenConnection();

                var cmd = new MySqlCommand(query, _connection);
                cmd.Parameters.AddWithValue("@packageId", packageId);
                cmd.ExecuteNonQuery();

                CloseConnection();
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }
        }

        public void EnablePackageTracking(int packageId)
        {
            const string query = "UPDATE package SET Tracking = @True WHERE Id = @packageId";

            try
            {
                OpenConnection();

                var cmd = new MySqlCommand(query, _connection);
                cmd.Parameters.AddWithValue("@True", 1);
                cmd.Parameters.AddWithValue("@packageId", packageId);
                cmd.ExecuteNonQuery();

                CloseConnection();
            }
            catch(Exception ex)
            {
                throw ex.InnerException;
            }
        }

        public List<PackageRoute> ReturnRouteForPackage(int packageId)
        {
            const string query = "SELECT * FROM track where PackageId = @packageId";
            var route = new List<PackageRoute>();

            try
            {
                OpenConnection();

                var cmd = new MySqlCommand(query, _connection);
                cmd.Parameters.AddWithValue("@packageId", packageId);
                var dataReader = cmd.ExecuteReader();

                

                while (dataReader.Read())
                {
                    var packageRoute = new PackageRoute
                    {
                        PackageId = Convert.ToInt32(dataReader["PackageId"]),
                        City = Convert.ToString(dataReader["City"]),                        
                        Time = Convert.ToDateTime(dataReader["Time"]),                        
                    };

                    route.Add(packageRoute);
                }

                CloseConnection();

                return route.ToList();
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }
        }

        public void AddTrackingRouteForPackage(int packageId, string city, string dateTime)
        {
            const string query = "INSERT INTO track (PackageId, City, Time) VALUES (@_1, @_2,  @_3);";

            try
            {
                OpenConnection();

                var cmd = new MySqlCommand(query, _connection);
                cmd.Parameters.AddWithValue("@_1", packageId);
                cmd.Parameters.AddWithValue("@_2", city);
                cmd.Parameters.AddWithValue("@_3", dateTime);
                cmd.ExecuteNonQuery();

                CloseConnection();
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }
        }

        public string ReturnClientNameById(int clientId)
        {
            const string query = "SELECT Username FROM client WHERE Id = @clientId";
            var clientName = "";
            try
            {
                OpenConnection();

                var cmd = new MySqlCommand(query, _connection);
                cmd.Parameters.AddWithValue("@clientId", clientId);
                var dataReader = cmd.ExecuteReader();
               
                while (dataReader.Read())
                {
                    clientName = Convert.ToString(dataReader["Username"]);
                }

                CloseConnection();

                return clientName;
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }
        }

        public int ReturnClientIdByName(string clientName)
        {
            const string query = "SELECT Id FROM client WHERE Username = @clientName";
            try
            {
                OpenConnection();

                var cmd = new MySqlCommand(query, _connection);
                cmd.Parameters.AddWithValue("@clientName", clientName);
                var dataReader = cmd.ExecuteReader();

                var clientId = 0;
                while (dataReader.Read())
                {
                    clientId = Convert.ToInt32(dataReader["Id"]);
                }

                CloseConnection();

                return clientId;
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }
        }

        public List<string> ReturnAllClients()
        {
            const string query = "SELECT Username FROM client";
            var clientsList = new List<string>();
            
            try
            {
                OpenConnection();

                var cmd = new MySqlCommand(query, _connection);
            
                var dataReader = cmd.ExecuteReader();

                while (dataReader.Read())
                {
                    var client = Convert.ToString(dataReader["Username"]);

                    clientsList.Add(client);
                }

                CloseConnection();

                return clientsList;
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }
        }
    }
}