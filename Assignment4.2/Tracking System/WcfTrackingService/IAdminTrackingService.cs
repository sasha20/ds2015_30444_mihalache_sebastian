﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Web.ModelBinding;
using System.Web.Routing;

namespace WcfTrackingService
{    
    [ServiceContract]
    public interface IAdminTrackingService
    {
        [OperationContract]
        bool IsAdminLogonValid(string username, string password);

        [OperationContract]
        List<Package> GetPackages();

        [OperationContract]
        Package GetPackageById(int packageId);

        [OperationContract]
        void AddPackage(Package package);

        [OperationContract]
        void RemovePackage(int packageId);
      
        [OperationContract]
        void RegisterPackage(int packageId);

        [OperationContract]
        List<PackageRoute> GetPackageRoute(int packageId);

        [OperationContract]
        void AddTrackingRoute(int packageId, string city, string dateTime);

        [OperationContract]
        string GetClientNameById(int clientId);

        [OperationContract]
        int GetClientIdByName(string clientName);

        [OperationContract]
        List<string> GetAllClients();
    }

    // Use a data contract to add composite types to service operations.
    [DataContract]
    public class Package
    {
        [DataMember] 
        public int Id;
            
        [DataMember]
        public int SenderId { get; set; }

        [DataMember]
        public int ReceiverId { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public string SenderCity { get; set; }

        [DataMember]
        public string ReceiverCity { get; set; } 

        [DataMember]
        public bool Tracking { get; set; }
    }

    [DataContract]
    public class PackageRoute
    {
        [DataMember]
        public int PackageId;

        [DataMember]
        public string City { get; set; }

        [DataMember]
        public DateTime Time { get; set; }
    }
}