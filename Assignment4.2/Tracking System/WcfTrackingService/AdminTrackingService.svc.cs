﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WcfTrackingService
{
    public class AdminTrackingService : IAdminTrackingService
    {
        private readonly DatabaseAccess _dbAccess = new DatabaseAccess();

        public bool IsAdminLogonValid(string username, string password) 
        {
            return _dbAccess.IsValidUser(username, password);
        }

        public List<Package> GetPackages()
        {
            return _dbAccess.ReturnAllPackages();
        }

        public Package GetPackageById(int packageId)
        {
            return _dbAccess.ReturnPackageById(packageId);
        }

        public void AddPackage(Package package)
        {
            _dbAccess.InsertPackage(package);
        }

        public void RemovePackage(int packageId)
        {
            _dbAccess.DeletePackage(packageId);
        }

        public void RegisterPackage(int packageId)
        {
            _dbAccess.EnablePackageTracking(packageId);
        }

        public List<PackageRoute> GetPackageRoute(int packageId)
        {
            return _dbAccess.ReturnRouteForPackage(packageId);
        }

        public void AddTrackingRoute(int packageId, string city, string dateTime)
        {
            _dbAccess.AddTrackingRouteForPackage(packageId, city, dateTime);
        }

        public string GetClientNameById(int clientId)
        {
            return _dbAccess.ReturnClientNameById(clientId);
        }

        public int GetClientIdByName(string clientName)
        {
            return _dbAccess.ReturnClientIdByName(clientName);
        }

        public List<string> GetAllClients()
        {
            return _dbAccess.ReturnAllClients();
        }
    }
}