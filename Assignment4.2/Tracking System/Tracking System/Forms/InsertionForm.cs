﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tracking_System.AdminTrackingServiceReference;

// ReSharper disable InconsistentNaming
// ReSharper disable ArrangeThisQualifier

namespace Tracking_System.Forms
{
    public partial class InsertionForm : Form
    {
        private readonly AdminTrackingServiceClient _srvClientRef = new AdminTrackingServiceClient();

        public InsertionForm()
        {
            InitializeComponent();
            PopulateClients();
        }

        private void PopulateClients()
        {
            string[] clients = _srvClientRef.GetAllClients();

            foreach (var client in clients)
            {
                comboBoxInsertSender.Items.Add(client);
                comboBoxInsertReceiver.Items.Add(client);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();            
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            var newPackage = new Package
            {
                SenderId     = _srvClientRef.GetClientIdByName(comboBoxInsertSender.SelectedItem.ToString()),
                ReceiverId   = _srvClientRef.GetClientIdByName(comboBoxInsertReceiver.SelectedItem.ToString()),
                Name         = txtBoxInsertName.Text,
                Description  = txtBoxInsertDescription.Text,
                SenderCity   = txtBoxInsertSenderCity.Text,
                ReceiverCity = txtBoxInsertDestCity.Text,
                Tracking     = false
            };

            _srvClientRef.AddPackage(newPackage);

            // Clear form
            txtBoxInsertName.Text           = string.Empty;
            txtBoxInsertDescription.Text    = string.Empty;
            txtBoxInsertDestCity.Text       = string.Empty;
            comboBoxInsertSender.Text       = string.Empty;
            txtBoxInsertSenderCity.Text     = string.Empty;
            comboBoxInsertReceiver.Text     = string.Empty;

            this.Close();
        }

        //-- Prevent the user from being able to move the window -- :)
        protected override void WndProc(ref Message message)
        {
            const int WM_SYSCOMMAND = 0x0112;
            const int SC_MOVE       = 0xF010;

            if (message.Msg == WM_SYSCOMMAND)
            {
                var command = message.WParam.ToInt32() & 0xfff0;

                if (command == SC_MOVE)
                {
                    return;
                }
            }

            base.WndProc(ref message);
        }
    }
}
