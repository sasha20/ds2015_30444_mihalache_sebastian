﻿namespace Tracking_System.Forms
{
    partial class InsertionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtBoxInsertName = new System.Windows.Forms.TextBox();
            this.txtBoxInsertDescription = new System.Windows.Forms.TextBox();
            this.txtBoxInsertSenderCity = new System.Windows.Forms.TextBox();
            this.txtBoxInsertDestCity = new System.Windows.Forms.TextBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.comboBoxInsertSender = new System.Windows.Forms.ComboBox();
            this.comboBoxInsertReceiver = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(30, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Sender";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(30, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Receiver";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(30, 113);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Name";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(30, 152);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "Description";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(30, 227);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 17);
            this.label5.TabIndex = 4;
            this.label5.Text = "Sender City";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(30, 266);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(98, 17);
            this.label6.TabIndex = 5;
            this.label6.Text = "Destination City";
            // 
            // txtBoxInsertName
            // 
            this.txtBoxInsertName.Location = new System.Drawing.Point(154, 110);
            this.txtBoxInsertName.Name = "txtBoxInsertName";
            this.txtBoxInsertName.Size = new System.Drawing.Size(214, 25);
            this.txtBoxInsertName.TabIndex = 8;
            // 
            // txtBoxInsertDescription
            // 
            this.txtBoxInsertDescription.Location = new System.Drawing.Point(154, 149);
            this.txtBoxInsertDescription.Multiline = true;
            this.txtBoxInsertDescription.Name = "txtBoxInsertDescription";
            this.txtBoxInsertDescription.Size = new System.Drawing.Size(214, 61);
            this.txtBoxInsertDescription.TabIndex = 9;
            // 
            // txtBoxInsertSenderCity
            // 
            this.txtBoxInsertSenderCity.Location = new System.Drawing.Point(154, 221);
            this.txtBoxInsertSenderCity.Name = "txtBoxInsertSenderCity";
            this.txtBoxInsertSenderCity.Size = new System.Drawing.Size(214, 25);
            this.txtBoxInsertSenderCity.TabIndex = 10;
            // 
            // txtBoxInsertDestCity
            // 
            this.txtBoxInsertDestCity.Location = new System.Drawing.Point(154, 263);
            this.txtBoxInsertDestCity.Name = "txtBoxInsertDestCity";
            this.txtBoxInsertDestCity.Size = new System.Drawing.Size(214, 25);
            this.txtBoxInsertDestCity.TabIndex = 11;
            // 
            // btnCancel
            // 
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Location = new System.Drawing.Point(252, 331);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 25);
            this.btnCancel.TabIndex = 12;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOK.Location = new System.Drawing.Point(86, 331);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 25);
            this.btnOK.TabIndex = 13;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // comboBoxInsertSender
            // 
            this.comboBoxInsertSender.FormattingEnabled = true;
            this.comboBoxInsertSender.Location = new System.Drawing.Point(154, 32);
            this.comboBoxInsertSender.Name = "comboBoxInsertSender";
            this.comboBoxInsertSender.Size = new System.Drawing.Size(214, 25);
            this.comboBoxInsertSender.TabIndex = 14;
            // 
            // comboBoxInsertReceiver
            // 
            this.comboBoxInsertReceiver.FormattingEnabled = true;
            this.comboBoxInsertReceiver.Location = new System.Drawing.Point(154, 71);
            this.comboBoxInsertReceiver.Name = "comboBoxInsertReceiver";
            this.comboBoxInsertReceiver.Size = new System.Drawing.Size(214, 25);
            this.comboBoxInsertReceiver.TabIndex = 15;
            // 
            // InsertionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(415, 391);
            this.Controls.Add(this.comboBoxInsertReceiver);
            this.Controls.Add(this.comboBoxInsertSender);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.txtBoxInsertDestCity);
            this.Controls.Add(this.txtBoxInsertSenderCity);
            this.Controls.Add(this.txtBoxInsertDescription);
            this.Controls.Add(this.txtBoxInsertName);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "InsertionForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Add a new package ...";
            this.TopMost = true;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtBoxInsertName;
        private System.Windows.Forms.TextBox txtBoxInsertDescription;
        private System.Windows.Forms.TextBox txtBoxInsertSenderCity;
        private System.Windows.Forms.TextBox txtBoxInsertDestCity;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.ComboBox comboBoxInsertSender;
        private System.Windows.Forms.ComboBox comboBoxInsertReceiver;
    }
}