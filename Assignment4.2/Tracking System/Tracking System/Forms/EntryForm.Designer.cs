﻿namespace Tracking_System.Forms
{
    partial class EntryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radioBtnAdmin = new System.Windows.Forms.RadioButton();
            this.radioBtnClient = new System.Windows.Forms.RadioButton();
            this.txtBoxUsername = new System.Windows.Forms.TextBox();
            this.txtBoxPasswd = new System.Windows.Forms.TextBox();
            this.lblUsername = new System.Windows.Forms.Label();
            this.lblPasswd = new System.Windows.Forms.Label();
            this.lblEntryFormHeader = new System.Windows.Forms.Label();
            this.btnLogin = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // radioBtnAdmin
            // 
            this.radioBtnAdmin.AutoSize = true;
            this.radioBtnAdmin.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioBtnAdmin.Location = new System.Drawing.Point(197, 240);
            this.radioBtnAdmin.Name = "radioBtnAdmin";
            this.radioBtnAdmin.Size = new System.Drawing.Size(71, 24);
            this.radioBtnAdmin.TabIndex = 4;
            this.radioBtnAdmin.TabStop = true;
            this.radioBtnAdmin.Text = "Admin";
            this.radioBtnAdmin.UseVisualStyleBackColor = true;
            // 
            // radioBtnClient
            // 
            this.radioBtnClient.AutoSize = true;
            this.radioBtnClient.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioBtnClient.Location = new System.Drawing.Point(116, 240);
            this.radioBtnClient.Name = "radioBtnClient";
            this.radioBtnClient.Size = new System.Drawing.Size(65, 24);
            this.radioBtnClient.TabIndex = 3;
            this.radioBtnClient.TabStop = true;
            this.radioBtnClient.Text = "Client";
            this.radioBtnClient.UseVisualStyleBackColor = true;
            // 
            // txtBoxUsername
            // 
            this.txtBoxUsername.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBoxUsername.Location = new System.Drawing.Point(75, 121);
            this.txtBoxUsername.Name = "txtBoxUsername";
            this.txtBoxUsername.Size = new System.Drawing.Size(234, 27);
            this.txtBoxUsername.TabIndex = 1;
            // 
            // txtBoxPasswd
            // 
            this.txtBoxPasswd.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBoxPasswd.Location = new System.Drawing.Point(75, 182);
            this.txtBoxPasswd.Name = "txtBoxPasswd";
            this.txtBoxPasswd.PasswordChar = '*';
            this.txtBoxPasswd.Size = new System.Drawing.Size(234, 27);
            this.txtBoxPasswd.TabIndex = 2;
            // 
            // lblUsername
            // 
            this.lblUsername.AutoSize = true;
            this.lblUsername.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsername.Location = new System.Drawing.Point(155, 94);
            this.lblUsername.Name = "lblUsername";
            this.lblUsername.Size = new System.Drawing.Size(75, 20);
            this.lblUsername.TabIndex = 4;
            this.lblUsername.Text = "Username";
            // 
            // lblPasswd
            // 
            this.lblPasswd.AutoSize = true;
            this.lblPasswd.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPasswd.Location = new System.Drawing.Point(157, 155);
            this.lblPasswd.Name = "lblPasswd";
            this.lblPasswd.Size = new System.Drawing.Size(70, 20);
            this.lblPasswd.TabIndex = 5;
            this.lblPasswd.Text = "Password";
            // 
            // lblEntryFormHeader
            // 
            this.lblEntryFormHeader.AutoSize = true;
            this.lblEntryFormHeader.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEntryFormHeader.Location = new System.Drawing.Point(78, 29);
            this.lblEntryFormHeader.Name = "lblEntryFormHeader";
            this.lblEntryFormHeader.Size = new System.Drawing.Size(241, 30);
            this.lblEntryFormHeader.TabIndex = 6;
            this.lblEntryFormHeader.Text = "Log Into The Application";
            // 
            // btnLogin
            // 
            this.btnLogin.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogin.Location = new System.Drawing.Point(75, 281);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(234, 29);
            this.btnLogin.TabIndex = 5;
            this.btnLogin.Text = "LogIn";
            this.btnLogin.UseVisualStyleBackColor = false;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // EntryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(384, 361);
            this.Controls.Add(this.btnLogin);
            this.Controls.Add(this.lblEntryFormHeader);
            this.Controls.Add(this.lblPasswd);
            this.Controls.Add(this.lblUsername);
            this.Controls.Add(this.txtBoxPasswd);
            this.Controls.Add(this.txtBoxUsername);
            this.Controls.Add(this.radioBtnClient);
            this.Controls.Add(this.radioBtnAdmin);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "EntryForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tracking System";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton radioBtnAdmin;
        private System.Windows.Forms.RadioButton radioBtnClient;
        private System.Windows.Forms.TextBox txtBoxUsername;
        private System.Windows.Forms.TextBox txtBoxPasswd;
        private System.Windows.Forms.Label lblUsername;
        private System.Windows.Forms.Label lblPasswd;
        private System.Windows.Forms.Label lblEntryFormHeader;
        private System.Windows.Forms.Button btnLogin;
    }
}

