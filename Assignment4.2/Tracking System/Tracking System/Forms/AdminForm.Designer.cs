﻿namespace Tracking_System.Forms
{
    partial class AdminForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.listBoxViewAllPackages = new System.Windows.Forms.ListBox();
            this.groupBoxPackageDetails = new System.Windows.Forms.GroupBox();
            this.txtBoxIsTracked = new System.Windows.Forms.TextBox();
            this.txtBoxSenderCity = new System.Windows.Forms.TextBox();
            this.txtBoxDestinationCity = new System.Windows.Forms.TextBox();
            this.txtBoxDescription = new System.Windows.Forms.TextBox();
            this.txtBoxReceiver = new System.Windows.Forms.TextBox();
            this.txtBoxSender = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBoxRemovePackage = new System.Windows.Forms.GroupBox();
            this.btnRemovePackage = new System.Windows.Forms.Button();
            this.groupBoxRegisterPackage = new System.Windows.Forms.GroupBox();
            this.btnRegisterPackage = new System.Windows.Forms.Button();
            this.txtBoxSearchPackage = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.operationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addPackageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBoxPackageStatus = new System.Windows.Forms.GroupBox();
            this.dataGridViewTrackingRoute = new System.Windows.Forms.DataGridView();
            this.txtBoxRouteCity = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.btnAddNewRoute = new System.Windows.Forms.Button();
            this.groupBoxAddRoute = new System.Windows.Forms.GroupBox();
            this.btnViewAllPackages = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.txtBoxAskRemove = new System.Windows.Forms.TextBox();
            this.txtBoxAskRegister = new System.Windows.Forms.TextBox();
            this.dateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.groupBoxPackageDetails.SuspendLayout();
            this.groupBoxRemovePackage.SuspendLayout();
            this.groupBoxRegisterPackage.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.groupBoxPackageStatus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTrackingRoute)).BeginInit();
            this.groupBoxAddRoute.SuspendLayout();
            this.SuspendLayout();
            // 
            // listBoxViewAllPackages
            // 
            this.listBoxViewAllPackages.BackColor = System.Drawing.Color.White;
            this.listBoxViewAllPackages.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listBoxViewAllPackages.FormattingEnabled = true;
            this.listBoxViewAllPackages.ItemHeight = 17;
            this.listBoxViewAllPackages.Location = new System.Drawing.Point(12, 79);
            this.listBoxViewAllPackages.Name = "listBoxViewAllPackages";
            this.listBoxViewAllPackages.Size = new System.Drawing.Size(162, 342);
            this.listBoxViewAllPackages.TabIndex = 0;
            this.listBoxViewAllPackages.SelectedIndexChanged += new System.EventHandler(this.listBoxViewAllPackages_SelectedIndexChanged);
            // 
            // groupBoxPackageDetails
            // 
            this.groupBoxPackageDetails.BackColor = System.Drawing.Color.White;
            this.groupBoxPackageDetails.Controls.Add(this.txtBoxIsTracked);
            this.groupBoxPackageDetails.Controls.Add(this.txtBoxSenderCity);
            this.groupBoxPackageDetails.Controls.Add(this.txtBoxDestinationCity);
            this.groupBoxPackageDetails.Controls.Add(this.txtBoxDescription);
            this.groupBoxPackageDetails.Controls.Add(this.txtBoxReceiver);
            this.groupBoxPackageDetails.Controls.Add(this.txtBoxSender);
            this.groupBoxPackageDetails.Controls.Add(this.label6);
            this.groupBoxPackageDetails.Controls.Add(this.label5);
            this.groupBoxPackageDetails.Controls.Add(this.label4);
            this.groupBoxPackageDetails.Controls.Add(this.label3);
            this.groupBoxPackageDetails.Controls.Add(this.label2);
            this.groupBoxPackageDetails.Controls.Add(this.label1);
            this.groupBoxPackageDetails.Location = new System.Drawing.Point(180, 36);
            this.groupBoxPackageDetails.Name = "groupBoxPackageDetails";
            this.groupBoxPackageDetails.Size = new System.Drawing.Size(503, 226);
            this.groupBoxPackageDetails.TabIndex = 1;
            this.groupBoxPackageDetails.TabStop = false;
            this.groupBoxPackageDetails.Text = "Package Details";
            // 
            // txtBoxIsTracked
            // 
            this.txtBoxIsTracked.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtBoxIsTracked.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBoxIsTracked.Location = new System.Drawing.Point(347, 158);
            this.txtBoxIsTracked.Name = "txtBoxIsTracked";
            this.txtBoxIsTracked.ReadOnly = true;
            this.txtBoxIsTracked.Size = new System.Drawing.Size(140, 25);
            this.txtBoxIsTracked.TabIndex = 11;
            this.txtBoxIsTracked.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtBoxSenderCity
            // 
            this.txtBoxSenderCity.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtBoxSenderCity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBoxSenderCity.Location = new System.Drawing.Point(347, 44);
            this.txtBoxSenderCity.Name = "txtBoxSenderCity";
            this.txtBoxSenderCity.ReadOnly = true;
            this.txtBoxSenderCity.Size = new System.Drawing.Size(140, 25);
            this.txtBoxSenderCity.TabIndex = 10;
            this.txtBoxSenderCity.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtBoxDestinationCity
            // 
            this.txtBoxDestinationCity.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtBoxDestinationCity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBoxDestinationCity.Location = new System.Drawing.Point(347, 101);
            this.txtBoxDestinationCity.Name = "txtBoxDestinationCity";
            this.txtBoxDestinationCity.ReadOnly = true;
            this.txtBoxDestinationCity.Size = new System.Drawing.Size(140, 25);
            this.txtBoxDestinationCity.TabIndex = 9;
            this.txtBoxDestinationCity.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtBoxDescription
            // 
            this.txtBoxDescription.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtBoxDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBoxDescription.Location = new System.Drawing.Point(88, 158);
            this.txtBoxDescription.Multiline = true;
            this.txtBoxDescription.Name = "txtBoxDescription";
            this.txtBoxDescription.ReadOnly = true;
            this.txtBoxDescription.Size = new System.Drawing.Size(140, 62);
            this.txtBoxDescription.TabIndex = 8;
            this.txtBoxDescription.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtBoxReceiver
            // 
            this.txtBoxReceiver.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtBoxReceiver.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBoxReceiver.Location = new System.Drawing.Point(88, 101);
            this.txtBoxReceiver.Name = "txtBoxReceiver";
            this.txtBoxReceiver.ReadOnly = true;
            this.txtBoxReceiver.Size = new System.Drawing.Size(140, 25);
            this.txtBoxReceiver.TabIndex = 7;
            this.txtBoxReceiver.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtBoxSender
            // 
            this.txtBoxSender.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtBoxSender.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBoxSender.Location = new System.Drawing.Point(88, 44);
            this.txtBoxSender.Name = "txtBoxSender";
            this.txtBoxSender.ReadOnly = true;
            this.txtBoxSender.Size = new System.Drawing.Size(140, 25);
            this.txtBoxSender.TabIndex = 6;
            this.txtBoxSender.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(241, 161);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 17);
            this.label6.TabIndex = 5;
            this.label6.Text = "Tracked";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(241, 104);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(98, 17);
            this.label5.TabIndex = 4;
            this.label5.Text = "Destination City";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(241, 47);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "Sender City";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 161);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Description";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 104);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Receiver";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Sender";
            // 
            // groupBoxRemovePackage
            // 
            this.groupBoxRemovePackage.Controls.Add(this.txtBoxAskRemove);
            this.groupBoxRemovePackage.Controls.Add(this.btnRemovePackage);
            this.groupBoxRemovePackage.Location = new System.Drawing.Point(180, 268);
            this.groupBoxRemovePackage.Name = "groupBoxRemovePackage";
            this.groupBoxRemovePackage.Size = new System.Drawing.Size(248, 189);
            this.groupBoxRemovePackage.TabIndex = 2;
            this.groupBoxRemovePackage.TabStop = false;
            this.groupBoxRemovePackage.Text = "Remove Selected Package";
            // 
            // btnRemovePackage
            // 
            this.btnRemovePackage.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnRemovePackage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRemovePackage.ForeColor = System.Drawing.Color.DarkRed;
            this.btnRemovePackage.Location = new System.Drawing.Point(153, 147);
            this.btnRemovePackage.Name = "btnRemovePackage";
            this.btnRemovePackage.Size = new System.Drawing.Size(75, 27);
            this.btnRemovePackage.TabIndex = 0;
            this.btnRemovePackage.Text = "Remove";
            this.btnRemovePackage.UseVisualStyleBackColor = false;
            this.btnRemovePackage.Click += new System.EventHandler(this.btnRemovePackage_Click);
            // 
            // groupBoxRegisterPackage
            // 
            this.groupBoxRegisterPackage.Controls.Add(this.txtBoxAskRegister);
            this.groupBoxRegisterPackage.Controls.Add(this.btnRegisterPackage);
            this.groupBoxRegisterPackage.Location = new System.Drawing.Point(434, 268);
            this.groupBoxRegisterPackage.Name = "groupBoxRegisterPackage";
            this.groupBoxRegisterPackage.Size = new System.Drawing.Size(249, 189);
            this.groupBoxRegisterPackage.TabIndex = 3;
            this.groupBoxRegisterPackage.TabStop = false;
            this.groupBoxRegisterPackage.Text = "Register Selected Package";
            // 
            // btnRegisterPackage
            // 
            this.btnRegisterPackage.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnRegisterPackage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRegisterPackage.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnRegisterPackage.Location = new System.Drawing.Point(158, 147);
            this.btnRegisterPackage.Name = "btnRegisterPackage";
            this.btnRegisterPackage.Size = new System.Drawing.Size(75, 27);
            this.btnRegisterPackage.TabIndex = 0;
            this.btnRegisterPackage.Text = "Register";
            this.btnRegisterPackage.UseVisualStyleBackColor = false;
            this.btnRegisterPackage.Click += new System.EventHandler(this.btnRegisterPackage_Click);
            // 
            // txtBoxSearchPackage
            // 
            this.txtBoxSearchPackage.Location = new System.Drawing.Point(12, 45);
            this.txtBoxSearchPackage.Name = "txtBoxSearchPackage";
            this.txtBoxSearchPackage.Size = new System.Drawing.Size(162, 25);
            this.txtBoxSearchPackage.TabIndex = 4;
            this.txtBoxSearchPackage.Text = "Search ...";
            this.txtBoxSearchPackage.TextChanged += new System.EventHandler(this.txtBoxSearchPackage_TextChanged);
            this.txtBoxSearchPackage.Enter += new System.EventHandler(this.txtBoxSearchPackage_Enter);
            this.txtBoxSearchPackage.Leave += new System.EventHandler(this.txtBoxSearchPackage_Leave);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.operationsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(998, 24);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.newToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // operationsToolStripMenuItem
            // 
            this.operationsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addPackageToolStripMenuItem});
            this.operationsToolStripMenuItem.Name = "operationsToolStripMenuItem";
            this.operationsToolStripMenuItem.Size = new System.Drawing.Size(77, 20);
            this.operationsToolStripMenuItem.Text = "Operations";
            // 
            // addPackageToolStripMenuItem
            // 
            this.addPackageToolStripMenuItem.Name = "addPackageToolStripMenuItem";
            this.addPackageToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.addPackageToolStripMenuItem.Text = "Add Package";
            this.addPackageToolStripMenuItem.Click += new System.EventHandler(this.addPackageToolStripMenuItem_Click);
            // 
            // groupBoxPackageStatus
            // 
            this.groupBoxPackageStatus.Controls.Add(this.dataGridViewTrackingRoute);
            this.groupBoxPackageStatus.Location = new System.Drawing.Point(689, 36);
            this.groupBoxPackageStatus.Name = "groupBoxPackageStatus";
            this.groupBoxPackageStatus.Size = new System.Drawing.Size(297, 226);
            this.groupBoxPackageStatus.TabIndex = 6;
            this.groupBoxPackageStatus.TabStop = false;
            this.groupBoxPackageStatus.Text = "Package Status";
            // 
            // dataGridViewTrackingRoute
            // 
            this.dataGridViewTrackingRoute.AllowUserToAddRows = false;
            this.dataGridViewTrackingRoute.AllowUserToDeleteRows = false;
            this.dataGridViewTrackingRoute.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dataGridViewTrackingRoute.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewTrackingRoute.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dataGridViewTrackingRoute.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewTrackingRoute.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewTrackingRoute.Location = new System.Drawing.Point(3, 21);
            this.dataGridViewTrackingRoute.MultiSelect = false;
            this.dataGridViewTrackingRoute.Name = "dataGridViewTrackingRoute";
            this.dataGridViewTrackingRoute.ReadOnly = true;
            this.dataGridViewTrackingRoute.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridViewTrackingRoute.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewTrackingRoute.Size = new System.Drawing.Size(291, 202);
            this.dataGridViewTrackingRoute.TabIndex = 2;
            // 
            // txtBoxRouteCity
            // 
            this.txtBoxRouteCity.Location = new System.Drawing.Point(62, 36);
            this.txtBoxRouteCity.Name = "txtBoxRouteCity";
            this.txtBoxRouteCity.Size = new System.Drawing.Size(207, 25);
            this.txtBoxRouteCity.TabIndex = 3;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(20, 39);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 17);
            this.label8.TabIndex = 6;
            this.label8.Text = "City";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(20, 87);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(36, 17);
            this.label9.TabIndex = 7;
            this.label9.Text = "Time";
            // 
            // btnAddNewRoute
            // 
            this.btnAddNewRoute.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnAddNewRoute.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddNewRoute.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnAddNewRoute.Location = new System.Drawing.Point(194, 147);
            this.btnAddNewRoute.Name = "btnAddNewRoute";
            this.btnAddNewRoute.Size = new System.Drawing.Size(75, 27);
            this.btnAddNewRoute.TabIndex = 1;
            this.btnAddNewRoute.Text = "Add";
            this.btnAddNewRoute.UseVisualStyleBackColor = false;
            this.btnAddNewRoute.Click += new System.EventHandler(this.btnAddNewRoute_Click);
            // 
            // groupBoxAddRoute
            // 
            this.groupBoxAddRoute.Controls.Add(this.dateTimePicker);
            this.groupBoxAddRoute.Controls.Add(this.btnAddNewRoute);
            this.groupBoxAddRoute.Controls.Add(this.label9);
            this.groupBoxAddRoute.Controls.Add(this.txtBoxRouteCity);
            this.groupBoxAddRoute.Controls.Add(this.label8);
            this.groupBoxAddRoute.Enabled = false;
            this.groupBoxAddRoute.Location = new System.Drawing.Point(689, 268);
            this.groupBoxAddRoute.Name = "groupBoxAddRoute";
            this.groupBoxAddRoute.Size = new System.Drawing.Size(294, 189);
            this.groupBoxAddRoute.TabIndex = 12;
            this.groupBoxAddRoute.TabStop = false;
            this.groupBoxAddRoute.Text = "Add Route For Selected Package";
            // 
            // btnViewAllPackages
            // 
            this.btnViewAllPackages.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnViewAllPackages.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewAllPackages.Location = new System.Drawing.Point(12, 428);
            this.btnViewAllPackages.Name = "btnViewAllPackages";
            this.btnViewAllPackages.Size = new System.Drawing.Size(162, 28);
            this.btnViewAllPackages.TabIndex = 13;
            this.btnViewAllPackages.Text = "View All Packages";
            this.btnViewAllPackages.UseVisualStyleBackColor = false;
            this.btnViewAllPackages.Click += new System.EventHandler(this.btnViewAllPackages_Click);
            // 
            // txtBoxAskRemove
            // 
            this.txtBoxAskRemove.BackColor = System.Drawing.Color.White;
            this.txtBoxAskRemove.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtBoxAskRemove.Enabled = false;
            this.txtBoxAskRemove.ForeColor = System.Drawing.Color.Black;
            this.txtBoxAskRemove.Location = new System.Drawing.Point(9, 53);
            this.txtBoxAskRemove.Multiline = true;
            this.txtBoxAskRemove.Name = "txtBoxAskRemove";
            this.txtBoxAskRemove.ReadOnly = true;
            this.txtBoxAskRemove.Size = new System.Drawing.Size(219, 65);
            this.txtBoxAskRemove.TabIndex = 1;
            this.txtBoxAskRemove.Text = "...";
            this.txtBoxAskRemove.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtBoxAskRegister
            // 
            this.txtBoxAskRegister.BackColor = System.Drawing.Color.White;
            this.txtBoxAskRegister.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtBoxAskRegister.Enabled = false;
            this.txtBoxAskRegister.ForeColor = System.Drawing.Color.Black;
            this.txtBoxAskRegister.Location = new System.Drawing.Point(6, 53);
            this.txtBoxAskRegister.Multiline = true;
            this.txtBoxAskRegister.Name = "txtBoxAskRegister";
            this.txtBoxAskRegister.ReadOnly = true;
            this.txtBoxAskRegister.Size = new System.Drawing.Size(227, 65);
            this.txtBoxAskRegister.TabIndex = 2;
            this.txtBoxAskRegister.Text = "...";
            this.txtBoxAskRegister.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // dateTimePicker
            // 
            this.dateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dateTimePicker.Location = new System.Drawing.Point(62, 81);
            this.dateTimePicker.Name = "dateTimePicker";
            this.dateTimePicker.Size = new System.Drawing.Size(207, 25);
            this.dateTimePicker.TabIndex = 8;
            this.dateTimePicker.Value = new System.DateTime(2015, 12, 25, 0, 0, 0, 0);
            // 
            // AdminForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(998, 464);
            this.Controls.Add(this.btnViewAllPackages);
            this.Controls.Add(this.groupBoxAddRoute);
            this.Controls.Add(this.groupBoxPackageStatus);
            this.Controls.Add(this.txtBoxSearchPackage);
            this.Controls.Add(this.groupBoxRegisterPackage);
            this.Controls.Add(this.groupBoxRemovePackage);
            this.Controls.Add(this.groupBoxPackageDetails);
            this.Controls.Add(this.listBoxViewAllPackages);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "AdminForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tracking System - Admin Mode";
            this.groupBoxPackageDetails.ResumeLayout(false);
            this.groupBoxPackageDetails.PerformLayout();
            this.groupBoxRemovePackage.ResumeLayout(false);
            this.groupBoxRemovePackage.PerformLayout();
            this.groupBoxRegisterPackage.ResumeLayout(false);
            this.groupBoxRegisterPackage.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBoxPackageStatus.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTrackingRoute)).EndInit();
            this.groupBoxAddRoute.ResumeLayout(false);
            this.groupBoxAddRoute.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBoxViewAllPackages;
        private System.Windows.Forms.GroupBox groupBoxPackageDetails;
        private System.Windows.Forms.GroupBox groupBoxRemovePackage;
        private System.Windows.Forms.GroupBox groupBoxRegisterPackage;
        private System.Windows.Forms.TextBox txtBoxSearchPackage;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.TextBox txtBoxIsTracked;
        private System.Windows.Forms.TextBox txtBoxSenderCity;
        private System.Windows.Forms.TextBox txtBoxDestinationCity;
        private System.Windows.Forms.TextBox txtBoxDescription;
        private System.Windows.Forms.TextBox txtBoxReceiver;
        private System.Windows.Forms.TextBox txtBoxSender;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnRemovePackage;
        private System.Windows.Forms.Button btnRegisterPackage;
        private System.Windows.Forms.GroupBox groupBoxPackageStatus;
        private System.Windows.Forms.DataGridView dataGridViewTrackingRoute;
        private System.Windows.Forms.Button btnAddNewRoute;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtBoxRouteCity;
        private System.Windows.Forms.ToolStripMenuItem operationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addPackageToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBoxAddRoute;
        private System.Windows.Forms.Button btnViewAllPackages;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.TextBox txtBoxAskRemove;
        private System.Windows.Forms.TextBox txtBoxAskRegister;
        private System.Windows.Forms.DateTimePicker dateTimePicker;

    }
}