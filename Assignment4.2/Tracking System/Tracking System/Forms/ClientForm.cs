﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Tracking_System.ClientTrackingServiceReference;

namespace Tracking_System.Forms
{
    public partial class ClientForm : Form
    {
        private readonly ClientServiceClient _srvClientRef = new ClientServiceClient();
        private int _selectedIndex;
        private int _loggedClientId = 1; //Sender of package

        public ClientForm()
        {
            InitializeComponent();
            InitializeRouteDataGridComponent();
        }

        public void PopulateAdditionInfoFileds()
        {            
            List<PackageItem> allPackages = _srvClientRef.getPackagesByClientId(_loggedClientId).ToList();

            // Search for the selected package in the List<> in order to populate fields
            PackageItem selectedPackageItem = allPackages.FirstOrDefault(p => p.id == _selectedIndex);
            
            //-- Package Details
            txtBoxSender.Text           = _srvClientRef.getClientNameById(selectedPackageItem.senderId);
            txtBoxReceiver.Text         = _srvClientRef.getClientNameById(selectedPackageItem.receiverId);
            txtBoxDescription.Text      = selectedPackageItem.description;
            txtBoxSenderCity.Text       = selectedPackageItem.senderCity;
            txtBoxDestinationCity.Text  = selectedPackageItem.receiverCity;
            txtBoxIsTracked.Text        = selectedPackageItem.tracking.ToString();

        }

        private void PopulatePackageRouteStatus()
        {           
            List<PackageRouteItem> packageRouteList = _srvClientRef.getPackageRouteForPackage(_selectedIndex).ToList();

            dataGridViewTrackingRoute.DataSource = packageRouteList;
        }

        private void PopulateAllPackagesView()
        {
            listBoxViewAllPackages.Items.Clear();
                       
            //Gets only the packages that belong to the authenticated client
            List<PackageItem> allPackages = _srvClientRef.getPackagesByClientId(_loggedClientId).ToList();

            foreach (var package in allPackages)
            {
                listBoxViewAllPackages.Items.Add(package);
            }

            listBoxViewAllPackages.DisplayMember = "Name";
        }

        private void InitializeRouteDataGridComponent()
        {
            dataGridViewTrackingRoute.ColumnCount = 2;
            dataGridViewTrackingRoute.AutoGenerateColumns = false;

            dataGridViewTrackingRoute.Columns[0].Name = @"City";
            dataGridViewTrackingRoute.Columns[0].HeaderText = @"City";
            dataGridViewTrackingRoute.Columns[0].DataPropertyName = @"City";
            dataGridViewTrackingRoute.Columns[0].Width = 228;
            dataGridViewTrackingRoute.Columns[0].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

            dataGridViewTrackingRoute.Columns[1].Name = @"Time";
            dataGridViewTrackingRoute.Columns[1].HeaderText = @"Time";
            dataGridViewTrackingRoute.Columns[1].DataPropertyName = @"Time";
            dataGridViewTrackingRoute.Columns[1].Width = 228;
            dataGridViewTrackingRoute.Columns[1].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }
        
        private void btnViewAllPackages_Click(object sender, System.EventArgs e)
        {
            PopulateAllPackagesView();            
        }

        private void listBoxViewAllPackages_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            // ---------- Get the ID of the selected package ----------
            var p = listBoxViewAllPackages.SelectedItem as PackageItem;
            if (p != null)
            {
                _selectedIndex = p.id;
            }
            // --------------------------------------------------------

            PopulateAdditionInfoFileds();

            if(p != null && p.tracking)
            {
                PopulatePackageRouteStatus();
            }
            else
            {
                dataGridViewTrackingRoute.DataSource = null;
            }
        }

        private void txtBoxSearchPackage_TextChanged(object sender, System.EventArgs e)
        {
            var searchTerm = txtBoxSearchPackage.Text;
            var index = listBoxViewAllPackages.FindString(searchTerm);

            listBoxViewAllPackages.ClearSelected();

            if (index < 0 && searchTerm != "Search ...")
            {
                toolTip1.Show("Package not found", txtBoxSearchPackage, 1500);
            }
            else
            {
                listBoxViewAllPackages.SelectedIndex = index;
            }
        }

        private void txtBoxSearchPackage_Enter(object sender, System.EventArgs e)
        {
            txtBoxSearchPackage.Text = string.Empty;
        }

        private void txtBoxSearchPackage_Leave(object sender, System.EventArgs e)
        {
            txtBoxSearchPackage.Text = @"Search ...";
        }

        private void exitToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            Close();
        }
    }
}