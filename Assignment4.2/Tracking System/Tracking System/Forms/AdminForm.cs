﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Tracking_System.AdminTrackingServiceReference;
// ReSharper disable ArrangeThisQualifier

namespace Tracking_System.Forms
{
    public partial class AdminForm : Form
    {
        private readonly AdminTrackingServiceClient _srvAdminRef = new AdminTrackingServiceClient();
        private int _selectedIndex;

        public AdminForm() 
        {
            InitializeComponent();
            InitializeRouteDataGridComponent();
        }

        public void PopulateAdditionInfoFileds()
        {
            var selectedPackage         = _srvAdminRef.GetPackageById(_selectedIndex);

            //-- Package Details
            txtBoxSender.Text           = _srvAdminRef.GetClientNameById(selectedPackage.SenderId);
            txtBoxReceiver.Text         = _srvAdminRef.GetClientNameById(selectedPackage.ReceiverId);
            txtBoxDescription.Text      = selectedPackage.Description;
            txtBoxSenderCity.Text       = selectedPackage.SenderCity;
            txtBoxDestinationCity.Text  = selectedPackage.ReceiverCity;
            txtBoxIsTracked.Text        = selectedPackage.Tracking.ToString();

            //-- Package Removing
            txtBoxAskRemove.Text        = @"Do you really wanna remove package " + selectedPackage.Name + @"?";

            //-- Package Registering
            if (selectedPackage.Tracking)
            {
                txtBoxAskRegister.Text      = @"Package " + selectedPackage.Name + @" is already registered for tracking.";
                btnRegisterPackage.Enabled  = false;

                //-- Enable Route Adding
                groupBoxAddRoute.Enabled    = true;
            }
            else
            {
                txtBoxAskRegister.Text      = @"Do you wanna register package "  + selectedPackage.Name + @" for tracking?";
                btnRegisterPackage.Enabled  = true;

                //-- Disable Route Adding
                groupBoxAddRoute.Enabled    = false;
            }            
        }

        private void PopulateAllPackagesView()
        {
            listBoxViewAllPackages.Items.Clear();

            List<Package> allPackages = _srvAdminRef.GetPackages().ToList();

            foreach (var package in allPackages)
            {
                listBoxViewAllPackages.Items.Add(package);
            }

            listBoxViewAllPackages.DisplayMember = "Name";
        }

        private void PopulatePackageRouteStatus()
        {
            List<PackageRoute> packageRouteList = _srvAdminRef.GetPackageRoute(_selectedIndex).ToList();
          
            dataGridViewTrackingRoute.DataSource = packageRouteList;            
        }

        private void InitializeRouteDataGridComponent()
        {
            dataGridViewTrackingRoute.ColumnCount           = 2;
            dataGridViewTrackingRoute.AutoGenerateColumns   = false;

            dataGridViewTrackingRoute.Columns[0].Name               = @"City";
            dataGridViewTrackingRoute.Columns[0].HeaderText         = @"City";
            dataGridViewTrackingRoute.Columns[0].DataPropertyName   = @"City";

            dataGridViewTrackingRoute.Columns[1].Name               = @"Time";
            dataGridViewTrackingRoute.Columns[1].HeaderText         = @"Time";
            dataGridViewTrackingRoute.Columns[1].DataPropertyName   = @"Time";
            dataGridViewTrackingRoute.Columns[1].Width              = 150;
        }

        private void btnViewAllPackages_Click(object sender, System.EventArgs e)
        {            
            PopulateAllPackagesView();
        }

        private void txtBoxSearchPackage_TextChanged(object sender, System.EventArgs e)
        {
            var searchTerm = txtBoxSearchPackage.Text;
            var index      = listBoxViewAllPackages.FindString(searchTerm);

            listBoxViewAllPackages.ClearSelected();

            if (index < 0 && searchTerm != "Search ...")
            {
                toolTip1.Show("Package not found", txtBoxSearchPackage, 1500);
            }
            else
            {
                listBoxViewAllPackages.SelectedIndex = index;
            }
        }

        private void listBoxViewAllPackages_SelectedIndexChanged(object sender, System.EventArgs e)
        {                             
            // ---------- Get the ID of the selected package ----------
            var p = listBoxViewAllPackages.SelectedItem as Package;
            if (p != null)
            {
                _selectedIndex = p.Id;
            }
            // --------------------------------------------------------

            PopulateAdditionInfoFileds(); 
            PopulatePackageRouteStatus();
        }

        private void btnRemovePackage_Click(object sender, System.EventArgs e)
        {            
            _srvAdminRef.RemovePackage(_selectedIndex);


            PopulateAllPackagesView();
        }

        private void btnRegisterPackage_Click(object sender, System.EventArgs e)
        {
            _srvAdminRef.RegisterPackage(_selectedIndex);

            PopulateAdditionInfoFileds();
        }

        private void btnAddNewRoute_Click(object sender, System.EventArgs e)
        {            
            if(_srvAdminRef.GetPackageById(_selectedIndex).Tracking)
            {                
                var stringDate = Convert.ToDateTime(dateTimePicker.Value).ToString("yyyy-MM-dd hh:mm:ss");                

                _srvAdminRef.AddTrackingRoute(_selectedIndex, txtBoxRouteCity.Text, stringDate);

                txtBoxRouteCity.Text = string.Empty;
                dateTimePicker.Value = DateTime.Now;

                PopulatePackageRouteStatus();
            }            
        }

        private void txtBoxSearchPackage_Enter(object sender, EventArgs e)
        {
            txtBoxSearchPackage.Text = string.Empty;
        }

        private void txtBoxSearchPackage_Leave(object sender, EventArgs e)
        {
            txtBoxSearchPackage.Text = @"Search ...";
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void addPackageToolStripMenuItem_Click(object sender, EventArgs e)
        {           
            var form = new InsertionForm();            
            form.ShowDialog();            
        }
    }
}