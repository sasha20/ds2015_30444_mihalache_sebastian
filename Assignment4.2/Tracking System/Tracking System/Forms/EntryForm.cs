﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Tracking_System.AdminTrackingServiceReference;
using Tracking_System.ClientTrackingServiceReference;

namespace Tracking_System.Forms
{
    public partial class EntryForm : Form
    {
        private readonly AdminTrackingServiceClient _srvAdminRef = new AdminTrackingServiceClient();
        private readonly ClientServiceClient _srvClientRef       = new ClientServiceClient();

        public EntryForm()
        {
            InitializeComponent();            
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            var username = txtBoxUsername.Text;
            var password = txtBoxPasswd.Text;

            if(radioBtnClient.Checked)
            {
                //Client credentials check - Java Service

                if (_srvClientRef.isValidClient(username, password))
                {
                    txtBoxUsername.Text = "";
                    txtBoxPasswd.Text = "";

                    var form = new ClientForm();
                    Hide();
                    form.ShowDialog();
                    Show(); // Close();
                }
                else
                {
                    MessageBox.Show(@"Wrong credentials!");
                }
            }
            else if (radioBtnAdmin.Checked)
            {
                //Login credentials check - .Net WCF Service
             
                if (_srvAdminRef.IsAdminLogonValid(username, password))
                {
                    txtBoxUsername.Text = "";
                    txtBoxPasswd.Text = "";

                    var form = new AdminForm();
                    Hide();
                    form.ShowDialog();
                    Show();
                }
                else
                {
                    MessageBox.Show(@"Wrong credentials!");
                }
            }
        }
    }
}
