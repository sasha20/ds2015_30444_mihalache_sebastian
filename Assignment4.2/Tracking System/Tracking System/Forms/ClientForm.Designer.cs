﻿namespace Tracking_System.Forms
{
    partial class ClientForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataGridViewTrackingRoute = new System.Windows.Forms.DataGridView();
            this.txtBoxIsTracked = new System.Windows.Forms.TextBox();
            this.txtBoxSenderCity = new System.Windows.Forms.TextBox();
            this.txtBoxDestinationCity = new System.Windows.Forms.TextBox();
            this.txtBoxDescription = new System.Windows.Forms.TextBox();
            this.txtBoxReceiver = new System.Windows.Forms.TextBox();
            this.txtBoxSender = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnViewAllPackages = new System.Windows.Forms.Button();
            this.groupBoxPackageStatus = new System.Windows.Forms.GroupBox();
            this.txtBoxSearchPackage = new System.Windows.Forms.TextBox();
            this.groupBoxPackageDetails = new System.Windows.Forms.GroupBox();
            this.listBoxViewAllPackages = new System.Windows.Forms.ListBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTrackingRoute)).BeginInit();
            this.groupBoxPackageStatus.SuspendLayout();
            this.groupBoxPackageDetails.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(698, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // dataGridViewTrackingRoute
            // 
            this.dataGridViewTrackingRoute.AllowUserToAddRows = false;
            this.dataGridViewTrackingRoute.AllowUserToDeleteRows = false;
            this.dataGridViewTrackingRoute.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dataGridViewTrackingRoute.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewTrackingRoute.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dataGridViewTrackingRoute.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewTrackingRoute.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewTrackingRoute.Location = new System.Drawing.Point(3, 21);
            this.dataGridViewTrackingRoute.MultiSelect = false;
            this.dataGridViewTrackingRoute.Name = "dataGridViewTrackingRoute";
            this.dataGridViewTrackingRoute.ReadOnly = true;
            this.dataGridViewTrackingRoute.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridViewTrackingRoute.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewTrackingRoute.Size = new System.Drawing.Size(497, 164);
            this.dataGridViewTrackingRoute.TabIndex = 2;
            // 
            // txtBoxIsTracked
            // 
            this.txtBoxIsTracked.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtBoxIsTracked.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBoxIsTracked.Location = new System.Drawing.Point(347, 158);
            this.txtBoxIsTracked.Name = "txtBoxIsTracked";
            this.txtBoxIsTracked.ReadOnly = true;
            this.txtBoxIsTracked.Size = new System.Drawing.Size(140, 25);
            this.txtBoxIsTracked.TabIndex = 11;
            this.txtBoxIsTracked.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtBoxSenderCity
            // 
            this.txtBoxSenderCity.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtBoxSenderCity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBoxSenderCity.Location = new System.Drawing.Point(347, 44);
            this.txtBoxSenderCity.Name = "txtBoxSenderCity";
            this.txtBoxSenderCity.ReadOnly = true;
            this.txtBoxSenderCity.Size = new System.Drawing.Size(140, 25);
            this.txtBoxSenderCity.TabIndex = 10;
            this.txtBoxSenderCity.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtBoxDestinationCity
            // 
            this.txtBoxDestinationCity.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtBoxDestinationCity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBoxDestinationCity.Location = new System.Drawing.Point(347, 101);
            this.txtBoxDestinationCity.Name = "txtBoxDestinationCity";
            this.txtBoxDestinationCity.ReadOnly = true;
            this.txtBoxDestinationCity.Size = new System.Drawing.Size(140, 25);
            this.txtBoxDestinationCity.TabIndex = 9;
            this.txtBoxDestinationCity.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtBoxDescription
            // 
            this.txtBoxDescription.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtBoxDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBoxDescription.Location = new System.Drawing.Point(88, 158);
            this.txtBoxDescription.Multiline = true;
            this.txtBoxDescription.Name = "txtBoxDescription";
            this.txtBoxDescription.ReadOnly = true;
            this.txtBoxDescription.Size = new System.Drawing.Size(140, 62);
            this.txtBoxDescription.TabIndex = 8;
            this.txtBoxDescription.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtBoxReceiver
            // 
            this.txtBoxReceiver.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtBoxReceiver.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBoxReceiver.Location = new System.Drawing.Point(88, 101);
            this.txtBoxReceiver.Name = "txtBoxReceiver";
            this.txtBoxReceiver.ReadOnly = true;
            this.txtBoxReceiver.Size = new System.Drawing.Size(140, 25);
            this.txtBoxReceiver.TabIndex = 7;
            this.txtBoxReceiver.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtBoxSender
            // 
            this.txtBoxSender.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtBoxSender.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBoxSender.Location = new System.Drawing.Point(88, 44);
            this.txtBoxSender.Name = "txtBoxSender";
            this.txtBoxSender.ReadOnly = true;
            this.txtBoxSender.Size = new System.Drawing.Size(140, 25);
            this.txtBoxSender.TabIndex = 6;
            this.txtBoxSender.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(241, 161);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 17);
            this.label6.TabIndex = 5;
            this.label6.Text = "Tracked";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(241, 104);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(98, 17);
            this.label5.TabIndex = 4;
            this.label5.Text = "Destination City";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(241, 47);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "Sender City";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 161);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Description";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 104);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Receiver";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Sender";
            // 
            // btnViewAllPackages
            // 
            this.btnViewAllPackages.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnViewAllPackages.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewAllPackages.Location = new System.Drawing.Point(12, 414);
            this.btnViewAllPackages.Name = "btnViewAllPackages";
            this.btnViewAllPackages.Size = new System.Drawing.Size(162, 28);
            this.btnViewAllPackages.TabIndex = 18;
            this.btnViewAllPackages.Text = "View All Packages";
            this.btnViewAllPackages.UseVisualStyleBackColor = false;
            this.btnViewAllPackages.Click += new System.EventHandler(this.btnViewAllPackages_Click);
            // 
            // groupBoxPackageStatus
            // 
            this.groupBoxPackageStatus.Controls.Add(this.dataGridViewTrackingRoute);
            this.groupBoxPackageStatus.Location = new System.Drawing.Point(180, 254);
            this.groupBoxPackageStatus.Name = "groupBoxPackageStatus";
            this.groupBoxPackageStatus.Size = new System.Drawing.Size(503, 188);
            this.groupBoxPackageStatus.TabIndex = 17;
            this.groupBoxPackageStatus.TabStop = false;
            this.groupBoxPackageStatus.Text = "Package Status";
            // 
            // txtBoxSearchPackage
            // 
            this.txtBoxSearchPackage.Location = new System.Drawing.Point(12, 31);
            this.txtBoxSearchPackage.Name = "txtBoxSearchPackage";
            this.txtBoxSearchPackage.Size = new System.Drawing.Size(162, 25);
            this.txtBoxSearchPackage.TabIndex = 16;
            this.txtBoxSearchPackage.Text = "Search ...";
            this.txtBoxSearchPackage.TextChanged += new System.EventHandler(this.txtBoxSearchPackage_TextChanged);
            this.txtBoxSearchPackage.Enter += new System.EventHandler(this.txtBoxSearchPackage_Enter);
            this.txtBoxSearchPackage.Leave += new System.EventHandler(this.txtBoxSearchPackage_Leave);
            // 
            // groupBoxPackageDetails
            // 
            this.groupBoxPackageDetails.BackColor = System.Drawing.Color.White;
            this.groupBoxPackageDetails.Controls.Add(this.txtBoxIsTracked);
            this.groupBoxPackageDetails.Controls.Add(this.txtBoxSenderCity);
            this.groupBoxPackageDetails.Controls.Add(this.txtBoxDestinationCity);
            this.groupBoxPackageDetails.Controls.Add(this.txtBoxDescription);
            this.groupBoxPackageDetails.Controls.Add(this.txtBoxReceiver);
            this.groupBoxPackageDetails.Controls.Add(this.txtBoxSender);
            this.groupBoxPackageDetails.Controls.Add(this.label6);
            this.groupBoxPackageDetails.Controls.Add(this.label5);
            this.groupBoxPackageDetails.Controls.Add(this.label4);
            this.groupBoxPackageDetails.Controls.Add(this.label3);
            this.groupBoxPackageDetails.Controls.Add(this.label2);
            this.groupBoxPackageDetails.Controls.Add(this.label1);
            this.groupBoxPackageDetails.Location = new System.Drawing.Point(180, 22);
            this.groupBoxPackageDetails.Name = "groupBoxPackageDetails";
            this.groupBoxPackageDetails.Size = new System.Drawing.Size(503, 226);
            this.groupBoxPackageDetails.TabIndex = 15;
            this.groupBoxPackageDetails.TabStop = false;
            this.groupBoxPackageDetails.Text = "Package Details";
            // 
            // listBoxViewAllPackages
            // 
            this.listBoxViewAllPackages.BackColor = System.Drawing.Color.White;
            this.listBoxViewAllPackages.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listBoxViewAllPackages.FormattingEnabled = true;
            this.listBoxViewAllPackages.ItemHeight = 17;
            this.listBoxViewAllPackages.Location = new System.Drawing.Point(12, 65);
            this.listBoxViewAllPackages.Name = "listBoxViewAllPackages";
            this.listBoxViewAllPackages.Size = new System.Drawing.Size(162, 342);
            this.listBoxViewAllPackages.TabIndex = 14;
            this.listBoxViewAllPackages.SelectedIndexChanged += new System.EventHandler(this.listBoxViewAllPackages_SelectedIndexChanged);
            // 
            // ClientForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(698, 464);
            this.Controls.Add(this.btnViewAllPackages);
            this.Controls.Add(this.groupBoxPackageStatus);
            this.Controls.Add(this.txtBoxSearchPackage);
            this.Controls.Add(this.groupBoxPackageDetails);
            this.Controls.Add(this.listBoxViewAllPackages);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "ClientForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tracking System - Client Mode";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTrackingRoute)).EndInit();
            this.groupBoxPackageStatus.ResumeLayout(false);
            this.groupBoxPackageDetails.ResumeLayout(false);
            this.groupBoxPackageDetails.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.DataGridView dataGridViewTrackingRoute;
        private System.Windows.Forms.TextBox txtBoxIsTracked;
        private System.Windows.Forms.TextBox txtBoxSenderCity;
        private System.Windows.Forms.TextBox txtBoxDestinationCity;
        private System.Windows.Forms.TextBox txtBoxDescription;
        private System.Windows.Forms.TextBox txtBoxReceiver;
        private System.Windows.Forms.TextBox txtBoxSender;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnViewAllPackages;
        private System.Windows.Forms.GroupBox groupBoxPackageStatus;
        private System.Windows.Forms.TextBox txtBoxSearchPackage;
        private System.Windows.Forms.GroupBox groupBoxPackageDetails;
        private System.Windows.Forms.ListBox listBoxViewAllPackages;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}