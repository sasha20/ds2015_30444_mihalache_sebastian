package service;

import java.sql.SQLException;

import dataLayer.DatabaseAccess;
import model.PackageItem;
import model.PackageRouteItem;

public class ClientService
{
	private DatabaseAccess	dbAccess	= new DatabaseAccess();

	public ClientService()
	{
	}

	public boolean isValidClient(String user, String password) throws SQLException
	{
		return dbAccess.isValidClient(user, password);
	}

	public String getClientNameById(int clientId) throws SQLException
	{
		return dbAccess.getClientNameById(clientId);
	}

	public int getClientIdByName(String clientName) throws SQLException
	{
		return dbAccess.getClientIdByName(clientName);
	}

	public PackageItem[] getPackagesByClientId(int clientId) throws SQLException
	{
		return dbAccess.getPackagesByClientId(clientId);
	}

	public PackageRouteItem[] getPackageRouteForPackage(int packageId) throws SQLException
	{
		return dbAccess.getPackageRouteForPackage(packageId);
	}
}
