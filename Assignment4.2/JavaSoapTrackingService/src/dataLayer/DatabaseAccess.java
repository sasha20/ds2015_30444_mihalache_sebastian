package dataLayer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.PackageItem;
import model.PackageRouteItem;

public class DatabaseAccess
{
	private String		url;
	private String		user;
	private String		passwd;
	private Connection	dbConnection;

	public DatabaseAccess()
	{
		initializeParams();
	}

	private void initializeParams()
	{
		try
		{
			Class.forName("com.mysql.jdbc.Driver");
		}
		catch (ClassNotFoundException ex)
		{

		}

		url = "jdbc:mysql://127.0.0.1:3306/ds_assign4_tracking";
		user = "root";
		passwd = "admin";
	}

	public boolean isValidClient(String username, String password) throws SQLException
	{
		String query = "SELECT Username FROM client WHERE Username = ? AND Password = ?";

		boolean status = false;

		dbConnection = DriverManager.getConnection(url, user, passwd);
		PreparedStatement state = dbConnection.prepareStatement(query);
		state.setString(1, username);
		state.setString(2, password);

		ResultSet result = state.executeQuery();

		int count = 0;

		while (result.next())
		{
			count += 1;
		}

		if (count == 1)
		{
			status = true;
		}

		result.close();
		dbConnection.close();

		return status;
	}

	public PackageItem[] getPackagesByClientId(int userId) throws SQLException
	{
		String query = "SELECT * FROM package WHERE SenderId = ?";

		dbConnection = DriverManager.getConnection(url, user, passwd);
		PreparedStatement state = dbConnection.prepareStatement(query);
		state.setInt(1, userId);

		ResultSet result = state.executeQuery();

		List<PackageItem> packageList = new ArrayList<>();

		while (result.next())
		{
			PackageItem pckg = new PackageItem();

			pckg.setId(result.getInt("Id"));
			pckg.setSenderId(result.getInt("SenderId"));
			pckg.setReceiverId(result.getInt("ReceiverId"));
			pckg.setName(result.getString("Name"));
			pckg.setDescription(result.getString("Description"));
			pckg.setSenderCity(result.getString("SenderCity"));
			pckg.setReceiverCity(result.getString("ReceiverCity"));
			pckg.setTracking(result.getBoolean("Tracking"));

			packageList.add(pckg);
		}

		PackageItem[] packageArray = packageList.toArray(new PackageItem[packageList.size()]);

		result.close();
		dbConnection.close();

		return packageArray;
	}

	public PackageRouteItem[] getPackageRouteForPackage(int packageId) throws SQLException
	{
		String query = "SELECT * FROM track WHERE PackageId = ?";

		dbConnection = DriverManager.getConnection(url, user, passwd);
		PreparedStatement state = dbConnection.prepareStatement(query);
		state.setInt(1, packageId);

		ResultSet result = state.executeQuery();

		List<PackageRouteItem> packageRouteList = new ArrayList<>();

		while (result.next())
		{
			PackageRouteItem packageRoute = new PackageRouteItem();

			packageRoute.setPackageId(result.getInt("PackageId"));
			packageRoute.setCity(result.getString("City"));
			packageRoute.setTime(result.getTimestamp("Time"));

			packageRouteList.add(packageRoute);
		}

		PackageRouteItem[] packageRouteArray = packageRouteList.toArray(new PackageRouteItem[packageRouteList.size()]);

		result.close();
		dbConnection.close();

		return packageRouteArray;
	}

	public String getClientNameById(int clientId) throws SQLException
	{
		String query = "SELECT Username FROM client WHERE Id = ?";

		dbConnection = DriverManager.getConnection(url, user, passwd);
		PreparedStatement state = dbConnection.prepareStatement(query);
		state.setInt(1, clientId);

		ResultSet result = state.executeQuery();

		String clientName = "";
		while (result.next())
		{
			clientName = result.getString("Username");
		}

		result.close();
		dbConnection.close();

		return clientName;
	}

	public int getClientIdByName(String clientName) throws SQLException
	{
		String query = "SELECT Id FROM client WHERE Username = ?";

		dbConnection = DriverManager.getConnection(url, user, passwd);
		PreparedStatement state = dbConnection.prepareStatement(query);
		state.setString(1, clientName);

		ResultSet result = state.executeQuery();

		int clientId = 0;
		while (result.next())
		{
			clientId = result.getInt("Id");
		}

		result.close();
		dbConnection.close();

		return clientId;
	}
}
