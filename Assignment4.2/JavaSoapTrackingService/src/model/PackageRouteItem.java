package model;

import java.io.Serializable;
import java.util.Date;

public class PackageRouteItem implements Serializable
{
	private static final long	serialVersionUID	= 1L;

	private int					PackageId;
	private String				City;
	private Date				Time;

	public PackageRouteItem()
	{
		super();
	}

	public int getPackageId()
	{
		return PackageId;
	}

	public void setPackageId(int packageId)
	{
		PackageId = packageId;
	}

	public String getCity()
	{
		return City;
	}

	public void setCity(String city)
	{
		City = city;
	}

	public Date getTime()
	{
		return Time;
	}

	public void setTime(Date time)
	{
		Time = time;
	}
}
