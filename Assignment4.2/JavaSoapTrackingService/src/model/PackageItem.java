package model;

import java.io.Serializable;

public class PackageItem implements Serializable
{
	private static final long	serialVersionUID	= 1L;

	private int					Id;
	private int					SenderId;
	private int					ReceiverId;
	private String				Name;
	private String				Description;
	private String				SenderCity;
	private String				ReceiverCity;
	private boolean				Tracking;

	public PackageItem()
	{
		super();
	}
	
	public int getId()
	{
		return Id;
	}

	public void setId(int id)
	{
		Id = id;
	}
	
	public int getSenderId()
	{
		return SenderId;
	}

	public void setSenderId(int senderId)
	{
		SenderId = senderId;
	}

	public int getReceiverId()
	{
		return ReceiverId;
	}

	public void setReceiverId(int receiverId)
	{
		ReceiverId = receiverId;
	}

	public String getName()
	{
		return Name;
	}

	public void setName(String name)
	{
		Name = name;
	}

	public String getDescription()
	{
		return Description;
	}

	public void setDescription(String description)
	{
		Description = description;
	}

	public String getSenderCity()
	{
		return SenderCity;
	}

	public void setSenderCity(String senderCity)
	{
		SenderCity = senderCity;
	}

	public String getReceiverCity()
	{
		return ReceiverCity;
	}

	public void setReceiverCity(String receiverCity)
	{
		ReceiverCity = receiverCity;
	}

	public boolean isTracking()
	{
		return Tracking;
	}

	public void setTracking(boolean tracking)
	{
		Tracking = tracking;
	}

	@Override
	public String toString()
	{
		return "[Id=" + Id + ", SenderId=" + SenderId + ", ReceiverId=" + ReceiverId + ", Name=" + Name
				+ ", Description=" + Description + ", SenderCity=" + SenderCity + ", ReceiverCity=" + ReceiverCity
				+ ", Tracking=" + Tracking + "]";
	}
}
