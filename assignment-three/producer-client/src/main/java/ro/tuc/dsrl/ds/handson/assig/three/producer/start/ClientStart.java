package ro.tuc.dsrl.ds.handson.assig.three.producer.start;

import ro.tuc.dsrl.ds.handson.assig.three.producer.connection.QueueServerConnection;
import ro.tuc.dsrl.ds.handson.assig.three.producer.service.DVDService;

import java.io.IOException;
import java.util.Scanner;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems, http://dsrl.coned.utcluj.ro/
 * @Module: assignment-one-client
 * @Since: Sep 1, 2015
 * @Description:
 *	Starting point for the Producer Client application. This
 *	application will send several messages to be inserted
 *  in the queue server (i.e. to be sent via email by a consumer).
 */
public class ClientStart {
	private static final String HOST = "localhost";
	private static final int PORT = 8888;

	private ClientStart() {
	}

	public static void main(String[] args) {
		QueueServerConnection queue = new QueueServerConnection(HOST, PORT);
		DVDService dvdService = new DVDService();
		Scanner scan = new Scanner(System.in);

		System.out.println("DVD Title:");
		dvdService.setTitle(scan.nextLine());
		System.out.println("DVD Year:");
		dvdService.setYear(scan.nextInt());
		System.out.println("DVD Price:");
		dvdService.setPrice(scan.nextDouble());

		dvdService.toString();
		String dvdData = "DVD Title: " + dvdService.getTitle() + "; DVD Year: " + String.valueOf(dvdService.getYear()) + "; DVD Price: " + String.valueOf(dvdService.getPrice());

		try {
//			for (int i=0;i<5;i++) {
//				queue.writeMessage("this is email number "+i);
//			}
			queue.writeMessage(dvdData);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
