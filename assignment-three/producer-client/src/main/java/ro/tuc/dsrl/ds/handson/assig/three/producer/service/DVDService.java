package ro.tuc.dsrl.ds.handson.assig.three.producer.service;

/**
 * Created by sasha.mihalache on 11/22/2015.
 */

public class DVDService {

    private String title;
    private int year;
    private double price;

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getTitle()
    {
        return title;
    }

    public void setYear(int year)
    {
        this.year = year;
    }

    public int getYear()
    {
        return year;
    }

    public void setPrice(double price)
    {
        this.price = price;
    }

    public double getPrice()
    {
        return price;
    }

    @Override
    public String toString() {
        return "DVDService{" +
                "title='" + title + '\'' +
                ", year=" + year +
                ", price=" + price +
                '}';
    }
}
