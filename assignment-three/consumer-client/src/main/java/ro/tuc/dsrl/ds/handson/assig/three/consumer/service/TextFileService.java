package ro.tuc.dsrl.ds.handson.assig.three.consumer.service;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

/**
 * Created by sasha.mihalache on 11/22/2015.
 */
public class TextFileService {

    final String message;

    public TextFileService(String message) throws FileNotFoundException{
        this.message = message;

        PrintWriter out = new PrintWriter("textfile.txt");
        out.println(message);
        out.close();
        System.out.println("File created.");
    }
}
