In order to run the Selling Price/Tax Calculator for cars, you must do the following:
	1)Import the project with the 4 different modules in Intellij IDEA or Eclipse
	(Originally, I've made the project in IDEA. There are 4 projects called modules that share dependencies
between them)
	2)After you've imported the 4 modules, set up the dependencies between them.
		Client needs CommonClasses and Interface
		Server requires also CommonClasses and Interface package.
	3)Now you're ready to start the application
		i) Start the ServerStart class with the main module. This will create a server that will listen
to incoming requests on port 11111.
		ii)Now you can either start Test in Client package for a console view of the application along with
2 results, one for Tax calculation and one for Selling Price based on some predefined inputs.
		ii)Or you can start the Client package with the ClientGUI class, which gives you a more user-friendly
experience to input desired values in the fields and see outputs based on the two buttons available.