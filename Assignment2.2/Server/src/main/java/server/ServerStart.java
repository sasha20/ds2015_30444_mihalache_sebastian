package main.java.server;

import main.java.interfaces.Constants;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * Created by sasha.mihalache on 11/8/2015.
 */
public class ServerStart  {
    public static void main(String args[])throws RemoteException, AlreadyBoundException{
        RemoteImplem implem = new RemoteImplem();
        Registry reg = LocateRegistry.createRegistry(Constants.RMI_PORT);
        reg.bind(Constants.RMI_ID,  implem);
        System.out.println("Server has started");
    }
}
