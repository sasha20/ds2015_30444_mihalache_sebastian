package main.java.server;

import main.java.interfaces.IRemote;
import main.java.models.Car;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * Created by sasha.mihalache on 11/8/2015.
 */
public class RemoteImplem extends UnicastRemoteObject implements IRemote{

    protected RemoteImplem() throws RemoteException{
        super();
    }

    public double calculateTax(Car car) throws RemoteException {
        int sum;
        if(car.getEngineCapacity()<1600)
            sum = 8;
        else if (car.getEngineCapacity()<2000)
            sum = 18;
        else if (car.getEngineCapacity()<2600)
            sum = 72;
        else if (car.getEngineCapacity()<3000)
            sum = 144;
        else sum = 290;

        double tax = (car.getEngineCapacity()/200)
                * sum;

        return tax;
    }

    public double calculateSellingPrice(Car car) throws RemoteException {
        double sellingPrice = car.getPrice()-(car.getPrice()/7)*(2015-car.getFabricationYear());
        return sellingPrice;
    }
}
