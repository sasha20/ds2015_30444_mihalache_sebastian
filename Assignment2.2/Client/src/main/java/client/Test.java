package main.java.client;

import main.java.interfaces.Constants;
import main.java.interfaces.IRemote;
import main.java.models.Car;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * Created by sasha.mihalache on 11/8/2015.
 */
public class Test {
    public static void main(String args[]) throws RemoteException, NotBoundException{
        Registry reg = LocateRegistry.getRegistry("localhost", Constants.RMI_PORT);
        IRemote remote = (IRemote) reg.lookup(Constants.RMI_ID);
        System.out.println(remote.calculateTax(new Car(2010, 2000, 25000.0)));
        System.out.println(remote.calculateSellingPrice(new Car(2010, 1600, 25000.0)));
    }
}
