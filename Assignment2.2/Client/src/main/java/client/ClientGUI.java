package main.java.client;

import main.java.interfaces.Constants;
import main.java.interfaces.IRemote;
import main.java.models.Car;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.text.DecimalFormat;

/**
 * Created by sasha.mihalache on 11/8/2015.
 */
public class ClientGUI {

    private JFrame frame;
    private JLabel label1;
    private JTextField fldYear;
    private JTextField fldPrice;
    private JTextField fldCap;
    private JLabel lblYear;
    private JLabel lblPurchasePrice;
    private JLabel lblEngineCapacity;
    private JButton btnCalculateTax;
    private JButton btnCalculateReselingPrice;
    private JTextField textField;

    public ClientGUI() throws RemoteException, NotBoundException {
        frame = new JFrame("Assignment 2.2");
        frame.setBounds(0,0 ,600,450);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        label1=new JLabel("Tax/Selling Price Calculator");
        label1.setFont(new Font("Dialog", Font.PLAIN, 20));
        label1.setBounds(180, 26, 389, 31);
        frame.getContentPane().add(label1);

        fldYear = new JTextField();
        fldYear.setBounds(199, 82, 330, 32);
        frame.getContentPane().add(fldYear);
        fldYear.setColumns(10);

        fldCap = new JTextField();
        fldCap.setBounds(199, 137, 330, 32);
        frame.getContentPane().add(fldCap);
        fldCap.setColumns(10);

        fldPrice = new JTextField();
        fldPrice.setBounds(199, 190,  330, 32);
        frame.getContentPane().add(fldPrice);
        fldPrice.setColumns(10);

        lblYear = new JLabel("Purchase Year:");
        lblYear.setBounds(31, 95, 169, 15);
        frame.getContentPane().add(lblYear);

        lblEngineCapacity = new JLabel("Engine Capacity:");
        lblEngineCapacity.setBounds(31, 151, 169, 15);
        frame.getContentPane().add(lblEngineCapacity);

        lblPurchasePrice = new JLabel("Purchase Price:");
        lblPurchasePrice.setBounds(31, 203, 188, 28);

        frame.getContentPane().add(lblPurchasePrice);

        btnCalculateTax = new JButton("Tax");
        btnCalculateTax.setBounds(22, 263, 120, 40);
        frame.getContentPane().add(btnCalculateTax);

        btnCalculateReselingPrice = new JButton("Re-sell Price");
        btnCalculateReselingPrice.setBounds(22, 323, 120, 40);
        frame.getContentPane().add(btnCalculateReselingPrice);

        textField = new JTextField();
        textField.setFont(new Font("Dialog", Font.PLAIN, 30));
        textField.setBounds(199, 260,330, 100);
        frame.getContentPane().add(textField);
        textField.setColumns(10);
        textField.setEditable(false);

        frame.setVisible(true);

        btnCalculateTax.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                double calculatedTax = 0;
                Registry registry = null;
                try {
                    registry = LocateRegistry.getRegistry("localhost", Constants.RMI_PORT);
                } catch (RemoteException e1) {
                   e1.printStackTrace();
                }
                IRemote remote = null;
                try {
                    remote = (IRemote)registry.lookup(Constants.RMI_ID);
                } catch (AccessException e1) {
                    e1.printStackTrace();
                } catch (RemoteException e1) {
                    e1.printStackTrace();
                } catch (NotBoundException e1) {
                    e1.printStackTrace();
                }

                String strCarYear     = fldYear.getText();
                String strCarPrice    = fldPrice.getText();
                String strCarCapacity = fldCap.getText();

                Car testCar = new Car(Integer.parseInt(strCarYear), Integer.parseInt(strCarCapacity),
                        Double.parseDouble(strCarPrice));
                try {
                    calculatedTax = remote.calculateTax(testCar);
                } catch (RemoteException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }

                textField.setText("Tax: "+calculatedTax + " RON");
            }
        });

        btnCalculateReselingPrice.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                double calculatedResellingPrice = 0;
                Registry registry = null;
                try {
                    registry = LocateRegistry.getRegistry("localhost", Constants.RMI_PORT);
                } catch (RemoteException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
                IRemote remote = null;
                try {
                    remote = (IRemote)registry.lookup(Constants.RMI_ID);
                } catch (AccessException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (RemoteException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (NotBoundException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }

                String strCarYear     = fldYear.getText();
                String strCarPrice    = fldPrice.getText();
                String strCarCapacity = fldCap.getText();
                Car testCar = new Car(Integer.parseInt(strCarYear), Integer.parseInt(strCarCapacity),
                        Double.parseDouble(strCarPrice));
                try {
                    calculatedResellingPrice = remote.calculateSellingPrice(testCar);
                } catch (RemoteException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
                //System.out.println(calculatedResellingPrice);
                if (calculatedResellingPrice>0) {
                    DecimalFormat df = new DecimalFormat("#.##");
                    textField.setText("PRICE: " + df.format(calculatedResellingPrice) + " RON");
                }

                else
                    textField.setText("CAR HAS NO VALUE!");
            }
        });
    }
    public static void main (String[] args) throws RemoteException, NotBoundException{
        ClientGUI g = new ClientGUI();
    }
}