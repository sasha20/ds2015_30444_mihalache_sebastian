package main.java.interfaces;

import main.java.models.Car;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by sasha.mihalache on 11/8/2015.
 */
public interface IRemote extends Remote {
    public double calculateTax(Car car) throws RemoteException;
    public double calculateSellingPrice(Car car) throws RemoteException;
}
