<%@ page import="main.java.com.daos.CityDAO" %>
<%@ page import="main.java.com.models.City" %>
<%@ page import="main.java.com.models.Flight" %>
<%@ page import="org.hibernate.cfg.Configuration" %>
<%@ page import="java.util.List" %>
<%--
  Created by IntelliJ IDEA.
  User: sasha.mihalache
  Date: 11/1/2015
  Time: 11:41 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Edit Page</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="css/bootstrap.min.js"></script>

</head>
<body>
<%
  //allow access only if session exists
  String user = null;
  if(session.getAttribute("user") == null){
    response.sendRedirect("index.html");
  }

  else if(session.getAttribute("role").equals("user")){
    response.sendRedirect("index.html");

  }
  else user = (String) session.getAttribute("user");
  String userName = null;
  String sessionID = null;
  Cookie[] cookies = request.getCookies();
  if(cookies !=null){
    for(Cookie cookie : cookies){
      if(cookie.getName().equals("user")) userName = cookie.getValue();
      if(cookie.getName().equals("JSESSIONID")) sessionID = cookie.getValue();
    }
  }

    Flight a = (Flight)request.getSession().getAttribute("toBeUpdated");
    System.out.print(a.getAirplaneType());

    CityDAO cDao = new CityDAO(new Configuration().configure().buildSessionFactory());

    List<City> cityList = cDao.findCities();


%>

    <h1>Edit Flight: <%= a.getFlightNumber() %>  </h1>
<div class="row">
    <div class="col-md-3">
        <form method="post" action="edit_flight">

            <div class="form-group">

                <input type="hidden" name="departure_city_id"  id="setDepId" class="departure_city_id" />
                <input type="hidden" name="arrival_city_id"  id="setArrId" class="departure_city_id" />

                <h4 class="form-horizontal"> Flight Number:</h4>
                <input type="text" name="flight_nb" class="form-control" value="<%=a.getFlightNumber() %>">


                <h4 class="form-horizontal"> Airplane Type:</h4>
                <input type="text" name="airplane_type" class="form-control" value="<%=a.getAirplaneType()%>">


                <h4 class="form-horizontal"> Departure Time: </h4>
                <input type="datetime-local" name="departure_time" class="form-control" value="<%=a.getDepartureTime()%>">

                <div class="form-group">
                    <h4 class="form-horizontal"> Departure City: </h4>
                    <div class="btn-group">

                        <a class="btn btn-default dropdown-toggle btn-select2 form-control"
                           data-toggle="dropdown" href="#">
                            Departure City <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" id="menu1">
                            <%

                                for(City city:cityList){
                            %>

                            <li>
                                <a href="#"><%=city.getId()%>:<%= city.getName() %></a>
                            </li>
                            <%
                                }
                            %>
                        </ul>
                    </div>
                </div>

                <h4 class="form-horizontal"> Arrival Time: </h4>
                <input type="datetime-local" name="arrival_time" class="form-control" value="<%=a.getArrivalTime()%>">

                <br />

                <div class="form-group">
                    <h4 class="form-horizontal"> Arrival City: </h4>
                    <div class="btn-group">

                        <a class="btn btn-default dropdown-toggle btn-select2 form-control"
                           data-toggle="dropdown" href="#">
                            Arrival City <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" id="menu2">
                            <%

                                for(City city:cityList){
                            %>

                            <li>
                                <a href="#"><%=city.getId()%>:<%= city.getName() %></a>
                            </li>
                            <%
                                }
                            %>
                        </ul>
                    </div>
                </div>
            </div>

            <input type="submit" class="btn btn-info pull-right" name="update_final" value="Update">
        </form>

    </div>
</div>

<script>
    $("#menu1 li a").click(function(){
        var selText = $(this).text();
        $(this).parents('.btn-group').find('.dropdown-toggle').html(selText+' <span class="caret"></span>');
        console.log(selText);

        var rez = selText.split(":");

        document.getElementById("setDepId").value = rez[0];
    });

    $("#menu2 li a").click(function(){
        var selText = $(this).text();
        $(this).parents('.btn-group').find('.dropdown-toggle').html(selText+' <span class="caret"></span>');
        console.log(selText);

        var rez = selText.split(":");

        document.getElementById("setArrId").value = rez[0];
    });




</script>
</body>
</html>
