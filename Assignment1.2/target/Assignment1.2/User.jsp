<%@ page import="main.java.com.daos.CityDAO" %>
<%@ page import="main.java.com.daos.FlightDAO" %>
<%@ page import="main.java.com.models.Flight" %>
<%@ page import="org.hibernate.cfg.Configuration" %>
<%@ page import="java.util.List" %>
<%@ page import="main.java.com.models.City" %>
<%--
  Created by IntelliJ IDEA.
  User: sasha.mihalache
  Date: 10/28/2015
  Time: 12:10 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=US-ASCII"
         pageEncoding="US-ASCII"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
  <title>User Page</title>
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="css/bootstrap.min.js"></script>

</head>
<body>
<%
  //allow access only if session exists
  String user = null;
  if(session.getAttribute("user") == null){
    response.sendRedirect("index.html");
  }else user = (String) session.getAttribute("user");
  String userName = null;
  String sessionID = null;
  Cookie[] cookies = request.getCookies();
  if(cookies !=null){
    for(Cookie cookie : cookies){
      if(cookie.getName().equals("user")) userName = cookie.getValue();
      if(cookie.getName().equals("JSESSIONID")) sessionID = cookie.getValue();
    }
  }
%>

<div class="jumbotron">
  <form action="LogoutServlet" method="post">
    <input type="submit" value="Logout" class="btn btn-default pull-right">
  </form>
  <h3>Hi, <%= userName %>! This is the User Panel</h3>
</div>

<%
  FlightDAO fDao = new FlightDAO(new Configuration().configure().buildSessionFactory());
  List<Flight> myList = null;
  myList = fDao.findFlights();
  Flight a = myList.get(0);

  CityDAO cDao = new CityDAO(new Configuration().configure().buildSessionFactory());

%>

<div class="row">
  <div class="col-lg-6">
  <table class="table" border="1">
    <thead>
    <tr>
      <th>Flight Number</th>
      <th>Airplane type </th>
      <th>Departure Date/Time</th>
      <th>Arrival Date/Time </th>
      <th>Departure city Name </th>
      <th>Arrival city name</th>
      <th>Check Local Time</th>
    </tr>
    </thead>
    <tbody>
    <%
      for (Flight flight : myList) {
    %>
    <TR>
      <TD> <%= flight.getFlightNumber()%> </TD>
      <TD> <%= flight.getAirplaneType()%> </TD>
      <TD> <%= flight.getDepartureTime()%> </TD>
      <TD> <%= flight.getArrivalTime()%> </TD>
      <TD> <%= cDao.findCity(flight.getDepartureCityId()).getName()  %>  </TD>
      <TD> <%= cDao.findCity(flight.getArrivalCityId()).getName()  %>  </TD>
      <TD><input type="button" class="btn btn-info" value="Call" onclick="displayTime(<%=cDao.findCity(flight.getArrivalCityId()).getLatitude()%>
              ,<%=cDao.findCity(flight.getArrivalCityId()).getLongitude()%>)"> </TD>
    </TR>
    <%
      }
    %>
    </tbody>
  </table>
    </div>
</div>

<script src="js/apiCall.js"></script>
</body>
</html>