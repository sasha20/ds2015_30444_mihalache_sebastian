package main.java.com.persistence;

import main.java.com.models.Flight;
import org.hibernate.Session;

import java.sql.Time;


/**
 * Created by sasha.mihalache on 10/29/2015.
 */
public class Test {
    public static void main( String[] args )
    {
        System.out.println("Maven + Hibernate + MySQL");
        Session session = HibernateUtil.getSessionFactory().openSession();

        session.beginTransaction();
//        User user = new User();
//        user.setFirstName("www");
//        user.setLastName("www");
//        user.setPassword("www");
//        user.setRole("www");
//        user.setUserName("www");
//        session.save(user);
//        session.getTransaction().commit();

//        City city = new City();
//        city.setName("Cluj-Napoca");
//        city.setLatitude(2321);
//        city.setLongitude(2123);
//        city.setLocalTime(new Time(System.currentTimeMillis()));
//        session.save(city);
//        session.getTransaction().commit();

        Flight flight = new Flight();
        flight.setDepartureCityId(1);
        flight.setArrivalCityId(2);
        flight.setAirplaneType("Boeing");
        flight.setFlightNumber("23");
        flight.setDepartureTime(new Time(System.currentTimeMillis()));
        flight.setArrivalTime(new Time(System.currentTimeMillis() ));

        session.save(flight);
        session.getTransaction().commit();
    }
}
