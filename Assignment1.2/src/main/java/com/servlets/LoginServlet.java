package main.java.com.servlets;


import main.java.com.daos.UserDAO;
import main.java.com.models.User;
import org.hibernate.cfg.Configuration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by sasha.mihalache on 10/27/2015.
 */
@WebServlet(name="LoginServlet")
public class LoginServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.getWriter().append("Served at: fmm ").append(request.getContextPath());

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
// TODO Auto-generated method stub
        String user =request.getParameter("userName");
        String pass =request.getParameter("password");

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            UserDAO uDAO = new UserDAO(new Configuration().configure().buildSessionFactory());
            User loginUser = new User();
            loginUser = uDAO.findUser(user, pass);

           if (loginUser!=null){

                HttpSession session = request.getSession();
                session.setAttribute("user", user);
                session.setAttribute("role", loginUser.getRole());
                // setting session to expiry in 20 mins
                session.setMaxInactiveInterval(20*60);
                Cookie userName = new Cookie("user", user);
                Cookie role = new Cookie("role", loginUser.getRole());
                userName.setMaxAge(30*60);
                role.setMaxAge(30*60);
                response.addCookie(userName);
                response.addCookie(role);
                if (loginUser.getRole().equals("admin")){
                    response.sendRedirect("Admin.jsp");
                }
                else if (loginUser.getRole().equals("user"))
                {
                    response.sendRedirect("User.jsp");
                }

            }
           else{

               out.print("<br> Bad username or password! <br />");
               out.print("<a href='index.html'> Go Back to Login </a>");
           }


        } finally {
            out.close();
        }

    }
}
