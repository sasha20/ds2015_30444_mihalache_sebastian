package main.java.com.servlets;

import main.java.com.daos.FlightDAO;
import main.java.com.models.Flight;
import org.hibernate.cfg.Configuration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by sasha.mihalache on 11/1/2015.
 */
@WebServlet(name = "EditServlet")
public class EditServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if(request.getParameter("update_final") != null) {

            PrintWriter out = response.getWriter();
            response.setContentType("text/html;charset=UTF-8");

            Flight a = (Flight)request.getSession().getAttribute("toBeUpdated");
            int idToUpdate = (int)request.getSession().getAttribute("idToUp");



            String flightNb=request.getParameter("flight_nb");
            String airplaneType=request.getParameter("airplane_type");
            String departureTime=request.getParameter("departure_time");
            String arrivalTime=request.getParameter("arrival_time");
            int departureCityId=Integer.parseInt(request.getParameter("departure_city_id"));
            int arrival_city_id=Integer.parseInt(request.getParameter("arrival_city_id"));

            Date dArr = new Date( );
            Date dDep = new Date( );



            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
            SimpleDateFormat output = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            try {
                dArr = sdf.parse(arrivalTime);
                dDep = sdf.parse(departureTime);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            Flight newFlight = new Flight(flightNb, airplaneType, departureCityId, arrival_city_id, dDep, dArr);
            FlightDAO fDao = new FlightDAO(new Configuration().configure().buildSessionFactory());
            fDao.updateFlight(fDao.findFlight(idToUpdate), newFlight);


            out.print("Successfully Updated!");
            out.print("<a href='Admin.jsp'> Go Back </a>");
        }



    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.getWriter().append("Served at: ").append(request.getContextPath());
    }
}
