package main.java.com.servlets;

import main.java.com.daos.FlightDAO;
import main.java.com.models.Flight;
import org.hibernate.cfg.Configuration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by sasha.mihalache on 10/31/2015.
 */
@WebServlet(name = "FlightServlet")
public class FlightServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        if (request.getParameter("delete_f")!=null){
            String idToDelete =request.getParameter("id");
            FlightDAO fDao = new FlightDAO(new Configuration().configure().buildSessionFactory());
            fDao.removeFlight(Integer.parseInt(idToDelete));
            PrintWriter out = response.getWriter();
            response.sendRedirect("Admin.jsp");
        }else if (request.getParameter("update_f")!=null){

            String idToUpdate = request.getParameter("id");
            FlightDAO fDao = new FlightDAO(new Configuration().configure().buildSessionFactory());

            Flight a = fDao.findFlight(Integer.parseInt(idToUpdate));

            System.out.println(a.getAirplaneType());

            request.getSession().setAttribute("idToUp", Integer.parseInt(idToUpdate));
            request.getSession().setAttribute("toBeUpdated", a);
            request.getRequestDispatcher("Edit.jsp").forward(request,response);



//            String idToUpdate = request.getParameter("id");
//            PrintWriter out = response.getWriter();
//            String flightNb=request.getParameter("flight_nb");
//            String airplaneType=request.getParameter("airplane_type");
//            String departureTime=request.getParameter("departure_time");
//            String arrivalTime=request.getParameter("arrival_time");
//            String departureCityId=request.getParameter("departure_city_id");
//            String arrival_city_id=request.getParameter("arrival_city_id");
//            out.println("Added!"+flightNb+", "+airplaneType+", "+departureTime
//                    +", "+arrivalTime+", "+departureCityId+", "+
//                    arrival_city_id);
//            Date dArr = new Date();
//            Date dDep = new Date();
//
//
//            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
//            SimpleDateFormat output = new SimpleDateFormat("yyyy-MM-dd HH:mm");
//            try {
//                dArr = sdf.parse(arrivalTime);
//                dDep = sdf.parse(departureTime);
//            } catch (ParseException e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
//
//            Flight newFlight = new Flight(flightNb, airplaneType, 1, 2, dDep, dArr);
//            FlightDAO fDao = new FlightDAO(new Configuration().configure().buildSessionFactory());
//            fDao.updateFlight(fDao.findFlight(Integer.parseInt(idToUpdate)), newFlight);
//
//            out.println("Updated!");
        }else if (request.getParameter("add_f")!=null){
            PrintWriter out = response.getWriter();

            String flightNb=request.getParameter("flight_nb");
            String airplaneType=request.getParameter("airplane_type");
            String departureTime=request.getParameter("departure_time");
            String arrivalTime=request.getParameter("arrival_time");
            int departureCityId = Integer.parseInt(request.getParameter("departure_city_id"));
            int arrival_city_id=Integer.parseInt(request.getParameter("arrival_city_id"));

            System.out.println(departureCityId);

            Date dArr = new Date();
            Date dDep = new Date();


            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            try {
                dArr = sdf.parse(arrivalTime);
                dDep = sdf.parse(departureTime);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            Flight newFlight = new Flight( flightNb, airplaneType, departureCityId, arrival_city_id, dDep, dArr);
            FlightDAO fDao = new FlightDAO(new Configuration().configure().buildSessionFactory());
            fDao.addFlight(newFlight);

            response.sendRedirect("Admin.jsp");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.getWriter().append("Served at: ").append(request.getContextPath());
    }
}
