package main.java.com.models;

import javax.persistence.*;

import java.util.Date;

/**
 * Created by sasha.mihalache on 10/31/2015.
 */
@Entity
@Table(name = "flight", schema = "", catalog = "assignment1")
public class Flight {
    private int id;
    private String flightNumber;
    private String airplaneType;
    private Date departureTime;
    private Date arrivalTime;
    private int departureCityId;
    private int arrivalCityId;

    public Flight(){};
    public Flight(int id, String flightNumber, String airplaneType, int departureCityId, int arrivalCityId, Date departureTime, Date arrivalTime){
        this.id = id;
        this.flightNumber = flightNumber;
        this.airplaneType = airplaneType;
        this.departureCityId = departureCityId;
        this.arrivalCityId = arrivalCityId;
        this.departureTime = departureTime;
        this.arrivalTime =arrivalTime;
    }
    public Flight(String flightNumber, String airplaneType, int departureCityId, int arrivalCityId, Date departureTime, Date arrivalTime){

        this.flightNumber = flightNumber;
        this.airplaneType = airplaneType;
        this.departureCityId = departureCityId;
        this.arrivalCityId = arrivalCityId;
        this.departureTime = departureTime;
        this.arrivalTime =arrivalTime;
    }

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "flight_number")
    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    @Basic
    @Column(name = "airplane_type")
    public String getAirplaneType() {
        return airplaneType;
    }

    public void setAirplaneType(String airplaneTypw) {
        this.airplaneType = airplaneTypw;
    }

    @Basic
    @Column(name = "departure_time")
    public Date getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Date departureTime) {
        this.departureTime = departureTime;
    }

    @Basic
    @Column(name = "arrival_time")
    public Date getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(Date arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    @Basic
    @Column(name = "departure_city_id")
    public int getDepartureCityId() {
        return departureCityId;
    }

    public void setDepartureCityId(int departureCityId) {
        this.departureCityId = departureCityId;
    }

    @Basic
    @Column(name = "arrival_city_id")
    public int getArrivalCityId() {
        return arrivalCityId;
    }

    public void setArrivalCityId(int arrivalCityId) {
        this.arrivalCityId = arrivalCityId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Flight that = (Flight) o;

        if (id != that.id) return false;
        if (departureCityId != that.departureCityId) return false;
        if (arrivalCityId != that.arrivalCityId) return false;
        if (flightNumber != null ? !flightNumber.equals(that.flightNumber) : that.flightNumber != null) return false;
        if (airplaneType != null ? !airplaneType.equals(that.airplaneType) : that.airplaneType != null) return false;
        if (departureTime != null ? !departureTime.equals(that.departureTime) : that.departureTime != null)
            return false;
        if (arrivalTime != null ? !arrivalTime.equals(that.arrivalTime) : that.arrivalTime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (flightNumber != null ? flightNumber.hashCode() : 0);
        result = 31 * result + (airplaneType != null ? airplaneType.hashCode() : 0);
        result = 31 * result + (departureTime != null ? departureTime.hashCode() : 0);
        result = 31 * result + (arrivalTime != null ? arrivalTime.hashCode() : 0);
        result = 31 * result + departureCityId;
        result = 31 * result + arrivalCityId;
        return result;
    }
}
