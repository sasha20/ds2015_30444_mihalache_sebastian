package main.java.com.daos;

import main.java.com.models.Flight;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.*;

import java.util.List;

/**
 * Created by sasha.mihalache on 10/31/2015.
 */
public class FlightDAO {
    private static final Log LOGGER = LogFactory.getLog(FlightDAO.class);

    private SessionFactory factory;

    public FlightDAO(SessionFactory factory) {
        this.factory = factory;
    }

    public Flight addFlight (Flight flight) {
        int flightId;
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            flightId = (Integer) session.save(flight);
            flight.setId(flightId);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
        return flight;
    }


    @SuppressWarnings("unchecked")
    public List<Flight> findFlights(){
        Session session = factory.openSession();
        Transaction tx = null;
        List<Flight> flights = null;
        try{
            tx = session.beginTransaction();
            flights = session.createQuery("FROM Flight").list();
            tx.commit();
        } catch (HibernateException e){
            if (tx != null){
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
        return flights;
    }

    @SuppressWarnings("unchecked")
    public Flight findFlight(int id){
        Session session = factory.openSession();
        Transaction tx = null;
        List<Flight> flights = null;
        try{
            tx=session.beginTransaction();
            Query query = session.createQuery("FROM Flight WHERE id = :id");
            query.setParameter("id", id);
            flights = query.list();
            tx.commit();
        } catch (HibernateException e){
            if (tx != null){
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
        return flights != null && !flights.isEmpty() ? flights.get(0):null;
    }


    @SuppressWarnings("unchecked")
    public boolean removeFlight(int id){
        boolean ok = false;
        Session session = factory.openSession();
        Transaction tx = null;
        List<Flight> flights = null;
        try{
            tx=session.beginTransaction();
            Query query = session.createQuery("FROM Flight WHERE id = :id");
            query.setParameter("id", id);
            flights = query.list();
            tx.commit();
        } catch (HibernateException e){
            if (tx != null){
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }

        session = factory.openSession();
        if (!flights.isEmpty())
            try{
                ok = true;
                tx=session.beginTransaction();
                session.delete(flights.get(0));
                tx.commit();
            } catch (HibernateException e){
                if (tx != null){
                    tx.rollback();
                }
                LOGGER.error("", e);
            }finally {
                session.close();
            }
        return ok;
    }

    public void updateFlight(Flight oldFlight, Flight newFlight)
    {
        Session session = factory.openSession();
        Transaction tx = null;
        try{
            tx=session.beginTransaction();
            Object obj = session.load(Flight.class, oldFlight.getId());
            Flight flight1 = (Flight)obj;
            flight1.setFlightNumber(newFlight.getFlightNumber());
            flight1.setAirplaneType(newFlight.getAirplaneType());
            flight1.setArrivalCityId(newFlight.getArrivalCityId());
            flight1.setDepartureCityId(newFlight.getDepartureCityId());
            flight1.setArrivalTime(newFlight.getArrivalTime());
            flight1.setDepartureTime(newFlight.getDepartureTime());
            session.update(flight1);
            tx.commit();
        } catch (HibernateException e)
        {
            if (tx != null){
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
    }



}