package main.java.com.daos;

import main.java.com.models.City;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.*;

import java.util.List;

/**
 * Created by sasha.mihalache on 10/31/2015.
 */
public class CityDAO {

    private static final Log LOGGER = LogFactory.getLog(CityDAO.class);
    private SessionFactory factory;

    public CityDAO(SessionFactory factory){
        this.factory=factory;
    }

    public City addCity(City city){
        int cityId = -1;
        org.hibernate.Session session = factory.openSession();
        org.hibernate.Transaction tx = null;
        try {
            tx=session.beginTransaction();
            cityId=(Integer)session.save(city);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }

        return city;
    }

    @SuppressWarnings("unchecked")
    public List<City> findCities(){
        Session session = factory.openSession();
        Transaction tx = null;
        List<City> cities = null;
        try{
            tx = session.beginTransaction();
            cities = session.createQuery("FROM City").list();
            tx.commit();
        } catch (HibernateException e){
            if (tx != null){
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
        return cities;
    }


    @SuppressWarnings("unchecked")
    public City findCity(int id){
        Session session = factory.openSession();
        Transaction tx = null;
        List<City> cities = null;
        try{
            tx=session.beginTransaction();
            Query query = session.createQuery("FROM City WHERE id = :idd");
            query.setParameter("idd", id);
            cities = query.list();
            tx.commit();
        } catch (HibernateException e){
            if (tx != null){
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
        return cities != null && !cities.isEmpty() ? cities.get(0):null;
    }


    @SuppressWarnings("unchecked")
    public boolean removeCity(int id){
        boolean ok = false;
        Session session = factory.openSession();
        Transaction tx = null;
        List<City> cities = null;
        try{
            tx=session.beginTransaction();
            Query query = session.createQuery("FROM City WHERE id = :id");
            query.setParameter("id", id);
            cities = query.list();
            tx.commit();
        } catch (HibernateException e){
            if (tx != null){
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }

        session = factory.openSession();
        if (!cities.isEmpty())
            try{
                ok = true;
                tx=session.beginTransaction();
                session.delete(cities.get(0));
                tx.commit();
            } catch (HibernateException e){
                if (tx != null){
                    tx.rollback();
                }
                LOGGER.error("", e);
            }finally {
                session.close();
            }
        return ok;
    }

    public void updateCity(City oldCity, City newCity)
    {
        Session session = factory.openSession();
        Transaction tx = null;
        try{
            tx=session.beginTransaction();
            Object obj = session.load(City.class, oldCity.getId());
            City city1 = (City)obj;
            city1.setName(newCity.getName());
            city1.setLatitude(newCity.getLatitude());
            city1.setLongitude(newCity.getLongitude());
            city1.setLocalTime(newCity.getLocalTime());
            session.update(city1);
            tx.commit();
        } catch (HibernateException e)
        {
            if (tx != null){
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
    }

}
