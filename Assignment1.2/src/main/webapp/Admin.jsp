<%@ page import="main.java.com.daos.CityDAO" %>
<%@ page import="main.java.com.daos.FlightDAO" %>
<%@ page import="main.java.com.models.Flight" %>
<%@ page import="org.hibernate.cfg.Configuration" %>
<%@ page import="java.util.List" %>
<%@ page import="main.java.com.models.City" %>
<%@ page import="java.io.PrintWriter" %>
<%--
  Created by IntelliJ IDEA.
  User: sasha.mihalache
  Date: 10/28/2015
  Time: 12:08 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=US-ASCII"
         pageEncoding="US-ASCII"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
  <title>Admin Page</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="css/bootstrap.min.js"></script>

</head>
<body>
<%
  //allow access only if session exists
  String user = null;
  if(session.getAttribute("user") == null){
    response.sendRedirect("index.html");
  }

  else if(session.getAttribute("role").equals("user")){
    response.sendRedirect("index.html");

  }
  else user = (String) session.getAttribute("user");
  String userName = null;
  String sessionID = null;
  Cookie[] cookies = request.getCookies();
  if(cookies !=null){
    for(Cookie cookie : cookies){
      if(cookie.getName().equals("user")) userName = cookie.getValue();
      if(cookie.getName().equals("JSESSIONID")) sessionID = cookie.getValue();
    }
  }
%>
<div class="jumbotron">
    <form action="LogoutServlet" method="post">
        <input type="submit" value="Logout" class="btn btn-default pull-right">
    </form>
<h3>Hi <%=userName %>, this is the admin panel.</h3>


</div>
<br>


<%
  FlightDAO fDao = new FlightDAO(new Configuration().configure().buildSessionFactory());
  List<Flight> myList = null;
  myList = fDao.findFlights();
  Flight a = myList.get(0);

  CityDAO cDao = new CityDAO(new Configuration().configure().buildSessionFactory());

    List<City> cityList = cDao.findCities();

%>

<div class="row">
    <div class="col-md-2 form-group">
    <form method="post" action="crud_flight">

        <div class="form-group">

            <input type="hidden" name="departure_city_id"  id="setDepId" class="departure_city_id" />
            <input type="hidden" name="arrival_city_id"  id="setArrId" class="departure_city_id" />


            <h4 class="form-horizontal"> Flight Number:</h4>
            <input type="text" name="flight_nb" class="form-control" placeholder="Flight Number">


            <h4 class="form-horizontal"> Airplane Type:</h4>
            <input type="text" name="airplane_type" class="form-control" placeholder="Plane Type">



            <h4 class="form-horizontal"> Departure Time: </h4>
            <input type="datetime-local" name="departure_time" class="form-control" placeholder="Departure Time">
            <br />

            <div class="form-group">
                <h4 class="form-horizontal"> Departure City: </h4>
                <div class="btn-group">

                    <a class="btn btn-default dropdown-toggle btn-select2 form-control"
                        data-toggle="dropdown" href="#">
                        Departure City <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" id="menu1">
                        <%

                            for(City city:cityList){
                        %>

                        <li>
                            <a href="#"><%=city.getId()%>:<%= city.getName() %></a>
                        </li>
                        <%
                            }
                        %>
                    </ul>
                </div>
            </div>

            <h4 class="form-horizontal"> Arrival Time: </h4>
            <input type="datetime-local" name="arrival_time" class="form-control" placeholder="Arrival Time">

            <br />

            <div class="form-group">
                <h4 class="form-horizontal"> Arrival City: </h4>
                <div class="btn-group">

                    <a class="btn btn-default dropdown-toggle btn-select2 form-control"
                       data-toggle="dropdown" href="#">
                        Arrival City <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" id="menu2">
                        <%

                            for(City city:cityList){
                        %>

                        <li>
                            <a href="#"><%=city.getId()%>:<%= city.getName() %></a>
                        </li>
                        <%
                            }
                        %>
                    </ul>
                </div>
            </div>
        </div>

      <input type="submit" class="btn btn-info pull-right" name="add_f" value="Add A New Flight">
    </form>



    </div>

    <div class="col-md-10">
    <table class="table" border="1">
        <thead>
        <tr>
            <th>Flight Number</th>
            <th>Airplane type </th>
            <th>Departure Date/Time</th>
            <th>Departure City</th>
            <th>Arrival Date/Time </th>
            <th>Arrival City</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        <%
            for (Flight flight : myList) {
        %>
        <TR>
            <TD> <%= flight.getFlightNumber()%> </TD>
            <TD> <%= flight.getAirplaneType()%> </TD>
            <TD> <%= flight.getDepartureTime()%> </TD>
            <TD> <%= cDao.findCity(flight.getDepartureCityId()).getName() %>  </TD>
            <TD> <%= flight.getArrivalTime()%> </TD>
            <TD> <%= cDao.findCity(flight.getArrivalCityId()).getName()  %>  </TD>
            <TD>
                <form method="post" action="crud_flight">
                    <input type="hidden" name="id"  value="<%=flight.getId() %>"/>
                    <input type="submit" class="btn btn-success" name="update_f" value="Update">
                </form>
                <form method="post" action="crud_flight">
                    <input type="hidden" name="id"  value="<%=flight.getId() %>"/>
                    <input type="submit" class="btn btn-danger" name="delete_f"  value="Delete" onclick="return confirm('Are you sure you want to delete this item?');" >
                </form>
            </TD>
        </TR>
        <%

            }
        %>
        </tbody>



    </table>
    </div>
</div>

<script>
    $("#menu1 li a").click(function(){
        var selText = $(this).text();
        $(this).parents('.btn-group').find('.dropdown-toggle').html(selText+' <span class="caret"></span>');
        console.log(selText);

        var rez = selText.split(":");

        document.getElementById("setDepId").value = rez[0];
    });

    $("#menu2 li a").click(function(){
        var selText = $(this).text();
        $(this).parents('.btn-group').find('.dropdown-toggle').html(selText+' <span class="caret"></span>');
        console.log(selText);

        var rez = selText.split(":");

        document.getElementById("setArrId").value = rez[0];
    });




</script>

</body>
</html>