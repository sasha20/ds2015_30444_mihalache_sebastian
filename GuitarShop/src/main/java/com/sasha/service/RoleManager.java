package com.sasha.service;

import com.sasha.model.Role;

import java.util.List;

/**
 * Created by sasha.mihalache on 12/6/2015.
 */
public interface RoleManager {
    public void addRole(Role role);
    public void deleteRole(Integer id);
    public Role findRoleById(Integer id);
    public List<Role> getAllRoles();
}






