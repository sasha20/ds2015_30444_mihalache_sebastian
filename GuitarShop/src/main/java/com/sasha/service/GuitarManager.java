package com.sasha.service;

import com.sasha.model.Guitar;

import java.util.List;

/**
 * Created by sasha.mihalache on 12/1/2015.
 */
public interface GuitarManager {

    public void addGuitar(Guitar guitar);
    public List<Guitar> getAllGuitars();
    public void deleteGuitar(Integer guitar_id);
    public Guitar findByGuitarId(Integer guitar_id);
    public void editGuitar(Guitar guitar);
}
