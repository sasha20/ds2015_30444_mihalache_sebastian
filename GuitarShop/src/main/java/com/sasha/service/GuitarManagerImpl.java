package com.sasha.service;

import com.sasha.dao.GuitarDAO;
import com.sasha.model.Guitar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by sasha.mihalache on 12/1/2015.
 */
@Service
public class GuitarManagerImpl implements GuitarManager
{
    @Autowired
    private GuitarDAO guitarDAO;

    @Override
    @Transactional
    public void addGuitar(Guitar guitar) {
        guitarDAO.addGuitar(guitar);
    }

    @Override
    @Transactional
    public List<Guitar> getAllGuitars() {
        return guitarDAO.getAllGuitars();
    }

    @Override
    @Transactional
    public void deleteGuitar(Integer guitar_id) {
        guitarDAO.deleteGuitar(guitar_id);
    }

    @Override
    @Transactional
    public Guitar findByGuitarId(Integer guitar_id) {
        return guitarDAO.findByGuitarId(guitar_id);
    }

    @Override
    @Transactional
    public void editGuitar(Guitar guitar) {
        guitarDAO.editGuitar(guitar);
    }
}
