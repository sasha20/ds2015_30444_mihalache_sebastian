package com.sasha.service;

import com.sasha.dao.RoleDAO;
import com.sasha.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by sasha.mihalache on 12/6/2015.
 */
@Service
public class RoleManagerImpl implements RoleManager{

    @Autowired
    private RoleDAO roleDAO;

    @Override
    @Transactional
    public void addRole(Role role) {
        roleDAO.addRole(role);
    }

    @Override
    @Transactional
    public void deleteRole(Integer id) {
        roleDAO.deleteRole(id);
    }

    @Override
    @Transactional
    public Role findRoleById(Integer id) {
        return roleDAO.findRoleById(id);
    }


    @Override
    @Transactional
    public List<Role> getAllRoles() {
        return roleDAO.getAllRoles();
    }


}
