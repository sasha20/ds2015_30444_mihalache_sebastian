package com.sasha.service;

import com.sasha.model.User;

import java.util.List;

/**
 * Created by sasha.mihalache on 11/21/2015.
 */
public interface UserManager {
    public void addUser(User user);
    public List<User> getAllUsers();
    public void deleteUser(String username);
    public User findByUsername(String username);
    public void editUser(User user);

}