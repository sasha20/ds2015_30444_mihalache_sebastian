package com.sasha.model;


import org.hibernate.Hibernate;

import javax.persistence.*;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.sql.Blob;
import java.sql.SQLException;


/**
 * Created by sasha.mihalache on 12/1/2015.
 */
@Entity
@Table(name="guitars")
public class Guitar implements Serializable{


    @Id
    @Column(name="guitar_id")
    @GeneratedValue
    private Integer guitar_id;

    @Column(name="brand")
    private String brand;

    @Column(name="name")
    private String name;

    @Column(name="type")
    private String type;

    @Column(name="price")
    private double price;

    @Column(name="color")
    private String color;

    @Column(name="sample")
    private Blob sample;

    @Column(name="image")
    private String image;

    @Column(name="quantity")
    private int quantity;

    public Guitar() {}

    public Guitar(String brand, String name, String type, double price, String color, Blob sample, String image, int quantity) {
        this.brand = brand;
        this.name = name;
        this.type = type;
        this.price = price;
        this.color = color;
        this.sample = sample;
        this.image = image;
        this.quantity = quantity;
    }
    public Guitar(String brand, String name, String type, double price, String color,   String image, int quantity) {
        this.brand = brand;
        this.name = name;
        this.type = type;
        this.price = price;
        this.color = color;
        this.image = image;
        this.quantity = quantity;
    }


    public Integer getGuitar_id() {
        return guitar_id;
    }

    public void setGuitar_id(Integer guitar_id) {
        this.guitar_id = guitar_id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Blob getSample() {

        return sample;
    }

    public void setSample(Blob sample) {
        this.sample = sample;
    }


//
//    public Blob getSampleBlob() {
//        return Hibernate.createBlob(this.sample);
//    }
//
//    public void setSampleBlob(Blob sample) {
//        this.sample = this.toByteArray(sample);
//    }
//
//    private byte[] toByteArray(Blob sample) {
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        try {
//            return toByteArrayImpl(sample, baos);
//        } catch (SQLException e) {
//            throw new RuntimeException(e);
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        } finally {
//            if (baos != null) {
//                try {
//                    baos.close();
//                } catch (IOException ex) {    }
//            }
//        }
//    }
//
//    private byte[] toByteArrayImpl(Blob sample, ByteArrayOutputStream baos)  throws SQLException, IOException {
//
//        byte[] buf = new byte[4000];
//        InputStream is = sample.getBinaryStream();
//        try {
//            for (;;) {
//                int dataSize = is.read(buf);
//                if (dataSize == -1)     break;
//                baos.write(buf, 0, dataSize);
//            }
//        }
//        finally
//        {
//            if (is != null) {
//
//                try {     is.close();    }
//                catch (IOException ex) {    }
//            }
//        }
//        return baos.toByteArray();
//    }


}
