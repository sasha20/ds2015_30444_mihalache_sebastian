package com.sasha.model;

/**
 * Created by sasha.mihalache on 12/27/2015.
 */
public class Item {

    private Guitar guitar = new Guitar();
    private int quantity;

    public Item(Guitar guitar, int quantity) {
        this.guitar = guitar;
        this.quantity = quantity;
    }

    public Guitar getGuitar() {
        return guitar;
    }

    public void setGuitar(Guitar guitar) {
        this.guitar = guitar;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
