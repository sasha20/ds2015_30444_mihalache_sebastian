package com.sasha.model;

import javax.persistence.*;

/**
 * Created by sasha.mihalache on 12/6/2015.
 */

@Entity
@Table(name="user_roles")
public class Role {

    @Id
    @Column(name="user_role_id")
    @GeneratedValue
    private Integer user_role_id;

    @Column(name="username" ,nullable = false)
    private String username;

    @Column(name="role")
    private String role;

    public Role() {}

    public Role(String username, String role) {
        this.username = username;
        this.role = role;
    }

    public Role(String username) {
        this.username = username;
    }

    public Integer getUser_role_id() {
        return user_role_id;
    }

    public void setUser_role_id(Integer user_role_id) {
        this.user_role_id = user_role_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
