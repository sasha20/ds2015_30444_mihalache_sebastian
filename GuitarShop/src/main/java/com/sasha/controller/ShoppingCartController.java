package com.sasha.controller;

import com.sasha.model.Item;
import com.sasha.service.GuitarManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sasha.mihalache on 12/27/2015.
 */

@Controller
@RequestMapping(value="/shoppingcart")
public class ShoppingCartController {

    @Autowired
    private GuitarManager guitarManager;

    @RequestMapping(value="/ordernow/{id}" , method= RequestMethod.GET)
    public String orderNow(@PathVariable (value="id") int id, HttpSession session)
    {
        if(session.getAttribute("cart") == null){
            List<Item> cart = new ArrayList<Item>();
            cart.add(new Item(this.guitarManager.findByGuitarId(id), 1));
            session.setAttribute("cart",cart);
        }
        else {
            List<Item> cart = (List<Item>)session.getAttribute("cart");
            int index = isExisting(id,session);
            if(index == -1){
                cart.add(new Item(this.guitarManager.findByGuitarId(id), 1));
            }
            else{
                int quantity = cart.get(index).getQuantity() + 1;
                cart.get(index).setQuantity(quantity);
            }
            session.setAttribute("cart",cart);
        }
        return "cart";
    }

    private int isExisting(int id, HttpSession session)
    {
        List<Item> cart = (List<Item>)session.getAttribute("cart");
        for(int i=0; i<cart.size(); i++){
            if(cart.get(i).getGuitar().getGuitar_id()== id)
                return i;

        }
        return -1;
    }

    @RequestMapping(value="/remove/{id}" , method= RequestMethod.GET)
    public String remove(@PathVariable (value="id") int id, HttpSession session){
        List<Item> cart = (List<Item>)session.getAttribute("cart");
        int index = isExisting(id,session);
        cart.remove(index);
        session.setAttribute("cart", cart);
        return "cart";
    }

    @RequestMapping(value="/place-order", method = RequestMethod.GET)
    public String placeOrder(HttpSession session) {
        List<Item> cart = (List<Item>)session.getAttribute("cart");
        cart.clear();
        return "placedorder";
    }
}
