package com.sasha.controller;

import com.sasha.model.Role;
import com.sasha.service.RoleManager;
import com.sasha.utility.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by sasha.mihalache on 12/28/2015.
 */
@Controller
public class RoleController {

    @Autowired
    private RoleManager roleManager;

    //    ROLE API
    @RequestMapping("/role")
    public @ResponseBody
    ResponseEntity<?> findRole(HttpSession session) {

        String userId = (String)session.getAttribute("userLogged");
        System.out.println(session.getAttribute("userLogged"));

        List<Role> roleList = roleManager.getAllRoles();
        int roleCounter = 0;
        Integer roleId = null;
        String roleType = "";

        for (Role role : roleList) {
            if (userId.equals(role.getUsername())) {
                roleCounter++;
                roleId = role.getUser_role_id();
                if(role.getRole().equals("ROLE_ADMIN"))
                    {   roleType = role.getRole();
                        break;
                    }
                else
                    roleType = role.getRole();
            }
        }

        if (roleType.equals("ROLE_ADMIN")) {
            return new ResponseEntity<Message>(new Message("Admin"), HttpStatus.OK);
        } else if (roleType.equals("ROLE_USER")) {
            return new ResponseEntity<Message>(new Message("User"), HttpStatus.OK);
        }
        return null;
    }
}
