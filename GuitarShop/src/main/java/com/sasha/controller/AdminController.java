package com.sasha.controller;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sasha.model.Guitar;
import com.sasha.model.Role;
import com.sasha.model.User;
import com.sasha.service.GuitarManager;
import com.sasha.service.RoleManager;
import com.sasha.service.UserManager;
import com.sasha.utility.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import sun.misc.BASE64Decoder;

import javax.servlet.http.HttpSession;
import javax.sql.rowset.serial.SerialBlob;
import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.Base64;
import java.util.List;

/**
 * Created by sasha.mihalache on 11/21/2015.
 */
@Controller
@RequestMapping(value="/admin")
public class AdminController
{
    @Autowired
    private UserManager userManager;
    @Autowired
    private GuitarManager guitarManager;
    @Autowired
    private RoleManager roleManager;

//    public void setUserManager(UserManager userManager) {
//        this.userManager = userManager;
//    }

    @RequestMapping( method = RequestMethod.GET)
    public String listContent(ModelMap map, HttpSession session)
    {

//        map.addAttribute("user", new User());
        map.addAttribute("userList", userManager.getAllUsers());

//        map.addAttribute("guitar", new Guitar());
        map.addAttribute("guitarList", guitarManager.getAllGuitars());

        return "index";
    }

//    @RequestMapping(value = "addUser")
//    public String goToAddUser()
//    {
//        return "AddUser";
//    }
//
//    @RequestMapping(value = "/addUser/add", method = RequestMethod.POST)
//    public String addUser(@RequestParam("username") String username,
//                          @RequestParam("password") String password,
//                          @RequestParam("firstname") String firstname,
//                          @RequestParam("lastname") String lastname,
//                          @RequestParam("email") String email,
//                          @RequestParam("telephone") String telephone)
//    {
//        User user = new User(username,password,firstname,lastname,email,telephone);
//        Role role = new Role(username,"ROLE_USER");
//
//        userManager.addUser(user);
//        roleManager.addRole(role);
//        return "redirect:/admin";
//    }
//
//    @RequestMapping("editUser/{userId}")
//    public String goToEditUser(@PathVariable("userId") String userId, ModelMap map)
//    {
//        User user = userManager.findByUsername(userId);
//        map.addAttribute("user", user);
//
//        return "EditUser";
//    }
//
//    @RequestMapping(value="/editUser/edit", method=RequestMethod.POST)
//    public String editUser(@ModelAttribute(value="user") User user)
//    {
//        userManager.editUser(user);
//
//        return "redirect:/admin";
//    }

//    @RequestMapping("/deleteUser/{userId}")
//    public String deleteUser(@PathVariable("userId") String userId)
//    {
//
//        List<Role> roleList =  roleManager.getAllRoles();
//        int roleCounter = 0 ;
//        Integer roleId = null;
//
//        for(Role role: roleList){
//            if(userId.equals(role.getUsername())){
//                roleCounter++;
//                roleId = role.getUser_role_id();
//            }
//        }
//
//        if(roleCounter == 0){
//            userManager.deleteUser(userId);
//        }
//        else if(roleCounter == 1){
//            roleManager.deleteRole(roleId);
//            userManager.deleteUser(userId);
//        }
//        else {
//            System.out.println("You cannot delete an administator of the SYSTEM.");
//        }
//
//        return "redirect:/admin";
//    }

//    @RequestMapping(value = "addGuitar")
//    public String goToAddGuitar( )
//    {
//        return "AddGuitar";
//    }

//    @RequestMapping(value = "addGuitar/add", method = RequestMethod.POST)
//    public String addGuitar(@RequestParam("brand") String brand,
//                            @RequestParam("name") String name,
//                            @RequestParam("type") String type,
//                            @RequestParam("price") Double price,
//                            @RequestParam("color") String color,
//                            @RequestParam("hiddenImage") String image,
//                            @RequestParam("quantity") Integer quantity,
//                            @RequestParam("audioToUpload") String base64blob) throws IOException ,SQLException
//    {
//        System.out.println(brand +" "+name+" "+type+" "+price+" "+color+" "+image+" "+quantity+" "+base64blob);
//
//        String filteredString = base64blob.substring(22, base64blob.length());
//
//        BASE64Decoder decoder = new BASE64Decoder();
//        byte[] decodedBytes = decoder.decodeBuffer(filteredString);
//        Blob blob = new SerialBlob(decodedBytes);
//
//        Guitar guitar = new Guitar(brand,name,type,price,color,blob,image,quantity);
//        guitarManager.addGuitar(guitar);
//
//        return "redirect:/admin#/guitar-management";
//    }

//    @RequestMapping("editGuitar/{guitarId}")
//    public String goToEditGuitar(@PathVariable("guitarId") Integer guitarId, ModelMap map) throws SQLException
//    {
//        Guitar guitar = guitarManager.findByGuitarId(guitarId);
//
//        byte[] blob = guitar.getSample().getBytes(1, (int) guitar.getSample().length());
//
//        String sample = Base64.getEncoder().encodeToString(blob);
//
//        map.addAttribute("guitar", guitar);
//        map.addAttribute("sampleSound", sample);
//
//        return "EditGuitar";
//    }

//    @RequestMapping(value="/editGuitar/edit", method=RequestMethod.POST)
//    public String editGuitar (@RequestParam("guitar_id") Integer id,
//                            @RequestParam("brand") String brand,
//                            @RequestParam("name") String name,
//                            @RequestParam("type") String type,
//                            @RequestParam("price") Double price,
//                            @RequestParam("color") String color,
//                            @RequestParam("hiddenImage") String image,
//                            @RequestParam("quantity") Integer quantity,
//                            @RequestParam("audioToUpload") String base64blob) throws IOException ,SQLException
//    {
//
//        String filteredString = base64blob.substring(22, base64blob.length());
//
//        BASE64Decoder decoder = new BASE64Decoder();
//        byte[] decodedBytes = decoder.decodeBuffer(filteredString);
//        Blob blob = new SerialBlob(decodedBytes);
//
//        Guitar guitar = guitarManager.findByGuitarId(id);
//        guitar.setBrand(brand);
//        guitar.setName(name);
//        guitar.setType(type);
//        guitar.setPrice(price);
//        guitar.setColor(color);
//        guitar.setImage(image);
//        guitar.setQuantity(quantity);
//        guitar.setSample(blob);
//
//        guitarManager.editGuitar(guitar);
//
//        return "redirect:/admin";
//    }

//    @RequestMapping("/deleteGuitar/{guitar_id}")
//    public String deleteGuitar(@PathVariable("guitar_id") Integer guitar_id)
//    {
//        guitarManager.deleteGuitar(guitar_id);
//        return "redirect:/admin";
//    }


//    @RequestMapping("play/{id}")
//    public String playSong(@PathVariable("id") Integer id, ModelMap map) throws SQLException
//    {
//        Guitar guitar = guitarManager.findByGuitarId(id);
//
//        byte[] blob = guitar.getSample().getBytes(1, (int) guitar.getSample().length());
//
//        String sample = Base64.getEncoder().encodeToString(blob);
//
//
//        map.addAttribute("sampleSound", sample);
//        map.addAttribute("guitar", guitar);
//        return "playSample";
//
//
//    }

//    USER API
//    GET
    @RequestMapping(value = "/user-management/{username}", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity getOne(@PathVariable("username") String username) {
        User user = userManager.findByUsername(username);

        if(user == null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(user, HttpStatus.OK);
    }

//    GET LIST
    @RequestMapping(value="/user-management", method = RequestMethod.GET)
    public @ResponseBody  ResponseEntity<List<User>> getAll(){

        List<User> list = userManager.getAllUsers();
        if(list.isEmpty()){
            return new ResponseEntity<List<User>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<User>>(list, HttpStatus.OK);
    }

//    CREATE
    @RequestMapping(value="/user-management", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<?> create(@RequestBody User user){
        System.out.println("all ok.");
        System.out.println( "JSON: " + user.getUsername() + " " + user.getPassword());

        if(user.getUsername() != null && user.getPassword() !=null && user.getEmail() != null && user.getFirstname() !=null
                && user.getLastname() != null && user.getTelephone() !=null) {

            User found = userManager.findByUsername(user.getUsername());
            if (found != null) {
                return new ResponseEntity<Message>(new Message("Username Already Exists"), HttpStatus.BAD_REQUEST);
            } else {
                Role role = new Role(user.getUsername(),"ROLE_USER");
                userManager.addUser(user);
                roleManager.addRole(role);
                return new ResponseEntity<Message>(new Message("Successfully Added New User"), HttpStatus.OK);
            }
        }
        return new ResponseEntity<Message>(new Message("Required fields are empty"), HttpStatus.BAD_REQUEST);
    }


//    EDIT
    @RequestMapping(value="/user-management/{username}", method = RequestMethod.PUT)
    public @ResponseBody ResponseEntity<?> edit(@PathVariable("username") String username, @RequestBody User user){

        User currentUser = userManager.findByUsername(username);

        if(currentUser == null){
            return new ResponseEntity<Message>(new Message("User not found"), HttpStatus.NOT_FOUND);
        }

        currentUser.setUsername(user.getUsername());
        currentUser.setPassword(user.getPassword());
        currentUser.setFirstname(user.getFirstname());
        currentUser.setLastname(user.getLastname());
        currentUser.setEmail(user.getEmail());
        currentUser.setTelephone(user.getTelephone());

        userManager.editUser(currentUser);
        return new ResponseEntity<Message>(new Message("Updated Successfully"), HttpStatus.OK);
    }


//    DELETE
    @RequestMapping(value="/user-management/{username}", method = RequestMethod.DELETE)
    public @ResponseBody ResponseEntity<?> deleteUser(@PathVariable("username") String username){

        User found = userManager.findByUsername(username);
        if(found !=null){

            List<Role> roleList =  roleManager.getAllRoles();
            int roleCounter = 0 ;
            Integer roleId = null;

            for(Role role: roleList){
                if(username.equals(role.getUsername())){
                    roleCounter++;
                    roleId = role.getUser_role_id();
                }
            }

            if(roleCounter == 0){
                userManager.deleteUser(username);
                return new ResponseEntity<Message>(new Message("Deleted User"), HttpStatus.OK);
            }
            else if(roleCounter == 1){
                roleManager.deleteRole(roleId);
                userManager.deleteUser(username);
                return new ResponseEntity<Message>(new Message("Deleted User & Role"), HttpStatus.OK);
            }
            else {
                return new ResponseEntity<Message>(new Message("You cannot delete an administator of the SYSTEM."), HttpStatus.OK);
            }
        }
        else {
            return new ResponseEntity<Message>(new Message("User Not Found"), HttpStatus.BAD_REQUEST);
        }
    }




//    GUITAR API

    //    GET
    @RequestMapping(value = "/guitar-management/{guitar_id}", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity getOneGuitar(@PathVariable("guitar_id") Integer guitar_id) {
        Guitar guitar = guitarManager.findByGuitarId(guitar_id);

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();


        System.out.println(gson);
        if(guitar == null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity(gson.toJson(guitar), HttpStatus.OK);
    }

    //    GET LIST
    @RequestMapping(value="/guitar-management", method = RequestMethod.GET)
    public @ResponseBody  ResponseEntity getAllGuitars(){

        List<Guitar> list = guitarManager.getAllGuitars();
        if(list.isEmpty()){
            return new ResponseEntity<List<Guitar>>(HttpStatus.NO_CONTENT);
        }
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        return new ResponseEntity(gson.toJson(list), HttpStatus.OK);
    }

    //ADD

    @RequestMapping(value = "/guitar-management/add", method = RequestMethod.POST)
    public String addGuitar(@RequestParam("brand") String brand,
                            @RequestParam("name") String name,
                            @RequestParam("type") String type,
                            @RequestParam("price") Double price,
                            @RequestParam("color") String color,
                            @RequestParam("hiddenImage") String image,
                            @RequestParam("quantity") Integer quantity,
                            @RequestParam("audioToUpload") String base64blob) throws IOException ,SQLException
    {
        System.out.println(brand +" "+name+" "+type+" "+price+" "+color+" "+image+" "+quantity+" "+base64blob);

        String filteredString = base64blob.substring(22, base64blob.length());

        BASE64Decoder decoder = new BASE64Decoder();
        byte[] decodedBytes = decoder.decodeBuffer(filteredString);
        Blob blob = new SerialBlob(decodedBytes);

        Guitar guitar = new Guitar(brand,name,type,price,color,blob,image,quantity);
        guitarManager.addGuitar(guitar);

        return "redirect:/admin#/guitar-management";
    }

    //EDIT
    @RequestMapping(value= "/guitar-management/edit/{guitarId}", method=RequestMethod.GET)
    public String goToEditGuitar(@PathVariable("guitarId") Integer guitarId, ModelMap map) throws SQLException
    {
        Guitar guitar = guitarManager.findByGuitarId(guitarId);

        byte[] blob = guitar.getSample().getBytes(1, (int) guitar.getSample().length());

        String sample = Base64.getEncoder().encodeToString(blob);

        map.addAttribute("guitar", guitar);
        map.addAttribute("sampleSound", sample);

        return "EditGuitar";
//        return "redirect:/admin#/guitar-management/edit";
    }

    //EDIT

    @RequestMapping(value="/guitar-management/edit", method=RequestMethod.POST)
    public String editGuitar (@RequestParam("guitar_id") Integer id,
                              @RequestParam("brand") String brand,
                              @RequestParam("name") String name,
                              @RequestParam("type") String type,
                              @RequestParam("price") Double price,
                              @RequestParam("color") String color,
                              @RequestParam("hiddenImage") String image,
                              @RequestParam("quantity") Integer quantity,
                              @RequestParam("audioToUpload") String base64blob) throws IOException ,SQLException
    {

        String filteredString = base64blob.substring(22, base64blob.length());

        BASE64Decoder decoder = new BASE64Decoder();
        byte[] decodedBytes = decoder.decodeBuffer(filteredString);
        Blob blob = new SerialBlob(decodedBytes);

        Guitar guitar = guitarManager.findByGuitarId(id);
        guitar.setBrand(brand);
        guitar.setName(name);
        guitar.setType(type);
        guitar.setPrice(price);
        guitar.setColor(color);
        guitar.setImage(image);
        guitar.setQuantity(quantity);
        guitar.setSample(blob);

        guitarManager.editGuitar(guitar);

        return "redirect:/admin#/guitar-management";
    }

    //play sample
    @RequestMapping(value="/guitar-management/play/{id}", method=RequestMethod.GET)
    public String playSong(@PathVariable("id") Integer id, ModelMap map) throws SQLException
    {
        Guitar guitar = guitarManager.findByGuitarId(id);

        byte[] blob = guitar.getSample().getBytes(1, (int) guitar.getSample().length());

        String sample = Base64.getEncoder().encodeToString(blob);


        map.addAttribute("sampleSound", sample);
        map.addAttribute("guitar", guitar);
        return "playSample";
//        return "redirect:/admin#/guitar-management/play";

    }


    //DELETE
    @RequestMapping(value="/guitar-management/{guitar_id}", method = RequestMethod.DELETE)
    public @ResponseBody ResponseEntity deleteGuitar(@PathVariable("guitar_id") Integer guitar_id)
    {
        Guitar guitar = guitarManager.findByGuitarId(guitar_id);

        if(guitar == null){
            return new ResponseEntity(new Message("Not Found"), HttpStatus.BAD_REQUEST);
        }

        guitarManager.deleteGuitar(guitar_id);
        return new ResponseEntity(new Message("Successfully Deleted"), HttpStatus.OK);
    }




}