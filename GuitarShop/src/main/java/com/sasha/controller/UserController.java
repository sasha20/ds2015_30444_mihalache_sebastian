package com.sasha.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sasha.model.Guitar;
import com.sasha.service.GuitarManager;
import com.sasha.service.RoleManager;
import com.sasha.service.UserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.sql.SQLException;
import java.util.Base64;
import java.util.List;

/**
 * Created by sasha.mihalache on 12/27/2015.
 */
@Controller
@RequestMapping(value="/user")
public class UserController {

    @Autowired
    private GuitarManager guitarManager;

    @Autowired
    private RoleManager roleManager;

    @Autowired
    private UserManager userManager;

    @RequestMapping(method = RequestMethod.GET)
    public String listContent(ModelMap map){

        map.addAttribute("guitarList", guitarManager.getAllGuitars());
        return "index";
    }

    @RequestMapping("/guitar-management/play/{id}")
    public String playSong(@PathVariable("id") Integer id, ModelMap map) throws SQLException
    {
        Guitar guitar = guitarManager.findByGuitarId(id);

        byte[] blob = guitar.getSample().getBytes(1, (int) guitar.getSample().length());

        String sample = Base64.getEncoder().encodeToString(blob);

        map.addAttribute("sampleSound", sample);
        map.addAttribute("guitar", guitar);
        return "playSampleUser";
    }

    @RequestMapping(value="/guitar-management", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity getAllGuitars(){

        List<Guitar> list = guitarManager.getAllGuitars();
        if(list.isEmpty()){
            return new ResponseEntity<List<Guitar>>(HttpStatus.NO_CONTENT);
        }
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        return new ResponseEntity(gson.toJson(list), HttpStatus.OK);
    }

}
