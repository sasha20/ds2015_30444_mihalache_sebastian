package com.sasha.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by sasha.mihalache on 11/15/2015.
 */
@Controller
public class LoginController {

    @RequestMapping("/")
    public ModelAndView getLoginForm(
            @RequestParam(required = false) String authfailed, String logout,
            String denied) {
        String message = "";
        if (authfailed != null) {
            message = "Invalid username of password, try again !";
        } else if (logout != null) {
            message = "Logged Out successfully, login again to continue !";
        } else if (denied != null) {
            message = "Access denied for this user !";
        }
        return new ModelAndView("login", "message", message);
    }

    @RequestMapping(value = "/login" )
    public String checkRoles( HttpServletRequest request, HttpSession session){
        System.out.println(request.isUserInRole("ROLE_ADMIN"));
        session.setAttribute("userLogged", request.getUserPrincipal().getName());
        if(request.isUserInRole("ROLE_ADMIN")) {

            return "redirect:/admin";
//            return "redirect:index.html";
        }
        else if(request.isUserInRole("ROLE_USER")){
            return "redirect:/user";
        }

        return "redirect:?denied";

    }

        @RequestMapping("user")
    public String getUserPage() {
        return "user";
    }

    @RequestMapping("admin")
    public String getAdminPage() {
        return "admin";
    }

    @RequestMapping("403page")
    public String get403denied() {
        return "redirect:?denied";
    }

}