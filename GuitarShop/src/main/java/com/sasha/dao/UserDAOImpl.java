package com.sasha.dao;

import com.sasha.model.User;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by sasha.mihalache on 11/21/2015.
 */
@Repository
public class UserDAOImpl implements UserDAO
{
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void addUser(User user) {
        this.sessionFactory.getCurrentSession().save(user);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<User> getAllUsers() {
        return this.sessionFactory.getCurrentSession().createQuery("from User").list();
    }
    @Override
    public void deleteUser(String username) {
        User user = (User) sessionFactory.getCurrentSession().load(
            User.class, username);
        if (null != user) {
            this.sessionFactory.getCurrentSession().delete(user);
        }
    }

    @Override
    public User findByUsername(String username) {
        User user = (User) sessionFactory.getCurrentSession().get(User.class, username);
        if(user != null){
            return user;
        }
        return null;
    }


    @Override
    public void editUser(User user) {
        this.sessionFactory.getCurrentSession().saveOrUpdate(user);
    }
}