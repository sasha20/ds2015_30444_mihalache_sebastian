package com.sasha.dao;

import com.sasha.model.Guitar;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by sasha.mihalache on 12/1/2015.
 */
@Repository
public class GuitarDAOImpl implements GuitarDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void addGuitar(Guitar guitar) {
        this.sessionFactory.getCurrentSession().save(guitar);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Guitar> getAllGuitars() {
        return this.sessionFactory.getCurrentSession().createQuery("from Guitar").list();
    }

    @Override
    public void deleteGuitar(Integer guitar_id) {
        Guitar guitar = (Guitar) sessionFactory.getCurrentSession().load(
                Guitar.class, guitar_id);
        if (null != guitar) {
            this.sessionFactory.getCurrentSession().delete(guitar);
        }
    }

    @Override
    public Guitar findByGuitarId(Integer guitar_id) {
        Guitar guitar = (Guitar) sessionFactory.getCurrentSession().get(Guitar.class, guitar_id);
        if(guitar != null) {
            return guitar;
        }

        return null;
    }

    @Override
    public void editGuitar(Guitar guitar) {
        this.sessionFactory.getCurrentSession().saveOrUpdate(guitar);
    }
}
