package com.sasha.dao;

import com.sasha.model.Role;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by sasha.mihalache on 12/6/2015.
 */
@Repository
public class RoleDAOImpl implements RoleDAO{

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void addRole(Role role) {
        this.sessionFactory.getCurrentSession().save(role);
    }

    @Override
    public void deleteRole(Integer id) {
        Role role = (Role) sessionFactory.getCurrentSession().load(
                Role.class, id);
        if(role != null) {
            this.sessionFactory.getCurrentSession().delete(role);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Role> getAllRoles() {
        return this.sessionFactory.getCurrentSession().createQuery("from Role").list();
    }

    @Override
    public Role findRoleById(Integer id) {
        Role role = (Role) sessionFactory.getCurrentSession().get(Role.class,id);
        if(role != null) {
            return role;
        }
        return null;
    }
}
