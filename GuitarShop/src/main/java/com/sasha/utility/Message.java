package com.sasha.utility;

/**
 * Created by sasha.mihalache on 12/28/2015.
 */
public class Message {
    private String data;

    public Message(String data) {
        this.data = data;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
