package com.sasha.utility;

/**
 * Created by sasha.mihalache on 12/28/2015.
 */
public class Login {

    private String username;
    private String password;

    public Login(){}
    public Login(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
