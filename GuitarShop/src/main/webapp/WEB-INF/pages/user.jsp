<%--
  Created by IntelliJ IDEA.
  User: sasha.mihalache
  Date: 11/15/2015
  Time: 10:08 PM
  To change this template use File | Settings | File Templates.
--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>
<html>
<head>
  <title>User Page</title>
</head>
<body>
<c:url value="/j_spring_security_logout" var="logoutUrl" />

  <h2>User | You are now logged in</h2>
  <h3><a href="${logoutUrl}">Logout</a></h3>

  <%--Tabel de chitari aci cu Buy option--%>
<h1>Browse Guitars </h1>

<c:if  test="${!empty guitarList}">
  <table class="data">
    <tr>
      <th>Image</th>
      <th>Brand</th>
      <th>Name</th>
      <th>Type</th>
      <th>Price</th>
      <th>Color</th>
      <th>Sound Sample</th>
      <th>Quantity</th>
      <th>Action</th>
    </tr>
    <c:forEach items="${guitarList}" var="guitars">
      <tr>
        <td><img src="${guitars.image.toString()}" height="200" width="350" /></td>
        <td>${guitars.brand}</td>
        <td>${guitars.name}</td>
        <td>${guitars.type} </td>
        <td>${guitars.price}</td>
        <td>${guitars.color}</td>
        <td><a href="/user/play/${guitars.guitar_id}">Play</a></td>
        <td>${guitars.quantity}</td>
        <td>
          <a href="${pageContext.request.contextPath}/shoppingcart/ordernow/${guitars.guitar_id}">Add To Cart</a>
        </td>
      </tr>
    </c:forEach>
  </table>
</c:if>
</body>
</html>