<%--
  Created by IntelliJ IDEA.
  User: sasha.mihalache
  Date: 12/6/2015
  Time: 9:35 PM
  To change this template use File | Settings | File Templates.
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<html>
<head>
  <title>Add User</title>
</head>
<body>


<h2>Add a User</h2>
<form method="post" action="addUser/add">
  <table>
    <tr>
      <td>
        <label for="username">Username: </label>
        <input type="text" id="username" name="username"/>
      </td>
    </tr>
    <tr>
      <td>
        <label for="password">Password: </label>
        <input type="text" id="password" name="password" />
      </td>
    </tr>
    <tr>
      <td>
        <label for="firstname">First Name: </label>
        <input type="text" id="firstname" name="firstname" />
      </td>
    </tr>
    <tr>
      <td>
        <label for="lastname">Last Name: </label>
        <input type="text" id="lastname" name="lastname"/>
      </td>
    </tr>
    <tr>
      <td>
        <label for="email">Email: </label>
        <input type="text" id="email" name="email" />
      </td>
    </tr>
    <tr>
      <td>
        <label for="telephone">Telephone:</label>
        <input type="text" id="telephone" name="telephone" />
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <input type="submit" value="Add"/>
      </td>
    </tr>
  </table>
</form>

<a href="/admin">Back</a>

</body>
</html>