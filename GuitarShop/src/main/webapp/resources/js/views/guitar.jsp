<%--
  Created by IntelliJ IDEA.
  User: sasha.mihalache
  Date: 12/28/2015
  Time: 1:59 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="container">
    <div class="row">
          <div class="col-md-12">

              <a class="btn btn-info float-right" href="#/guitar-management/add">Add A Guitar</a>

              <h2>List Of Guitars</h2>

              <table class="table table-striped">
                  <tr>
                      <th>Image</th>
                      <th>Brand</th>
                      <th>Name</th>
                      <th>Type</th>
                      <th>Price</th>
                      <th>Color</th>
                      <th>Sound Sample</th>
                      <th>Quantity</th>
                      <th>Action</th>
                  </tr>
                  <tbody>
                  <tr ng-repeat="guitar in vm.guitars">
                      <td><img src="{{guitar.image}}" height="100" width="250" /></td>
                      <td>{{guitar.brand}}</td>
                      <td>{{guitar.name}}</td>
                      <td>{{guitar.type}}</td>
                      <td>{{guitar.price}}</td>
                      <td>{{guitar.color}}</td>
                      <td><a href="admin/guitar-management/play/{{guitar.guitar_id}}">Play</a></td>
                      <td>{{guitar.quantity}}</td>
                      <td>
                          <a class="btn btn-info" href="admin/guitar-management/edit/{{guitar.guitar_id}}">Edit</a>
                          <%--<a class="btn btn-danger" href="admin/deleteGuitar/{{guitar.guitar_id}}">Delete</a>--%>
                          <button class="btn btn-danger" ng-click="vm.onDeleteGuitarClick(guitar.guitar_id)">Delete</button>
                      </td>
                  </tr>
                  </tbody>
              </table>
          </div>
    </div>

</div>