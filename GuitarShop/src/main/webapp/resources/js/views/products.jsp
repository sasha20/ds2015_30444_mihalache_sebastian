<%--
  Created by IntelliJ IDEA.
  User: sasha.mihalache
  Date: 12/29/2015
  Time: 3:42 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>
<div class="container">
  <div class="row">
    <div class="col-md-12">

      <h2>List Of Guitars</h2>

      <table class="table table-striped">
        <tr>
          <th>Image</th>
          <th>Brand</th>
          <th>Name</th>
          <th>Type</th>
          <th>Price</th>
          <th>Color</th>
          <th>Sound Sample</th>
          <th>Quantity</th>
          <th>Action</th>
        </tr>
        <tbody>
        <tr ng-repeat="guitar in vm.guitars">
          <td><img src="{{guitar.image}}" height="100" width="250" /></td>
          <td>{{guitar.brand}}</td>
          <td>{{guitar.name}}</td>
          <td>{{guitar.type}}</td>
          <td>{{guitar.price}}</td>
          <td>{{guitar.color}}</td>
          <td><a href="user/guitar-management/play/{{guitar.guitar_id}}">Play</a></td>
          <td>{{guitar.quantity}}</td>
          <td>
            <%--<a class="btn btn-info" href="admin/guitar-management/edit/{{guitar.guitar_id}}">Edit</a>--%>
            <%--<a class="btn btn-danger" href="admin/deleteGuitar/{{guitar.guitar_id}}">Delete</a>--%>
              <a class="btn btn-info" href="shoppingcart/ordernow/{{guitar.guitar_id}}"> ${pageContext.request.contextPath}Add To Cart</a>
          </td>
        </tr>
        </tbody>
      </table>
    </div>
  </div>

</div>