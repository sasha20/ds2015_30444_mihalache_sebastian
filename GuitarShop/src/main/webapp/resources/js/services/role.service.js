/**
 * Created by sasha.mihalache on 12/28/2015.
 */
'use strict';

App.factory('RoleRepo', ['$http', '$q', function($http, $q){

    var factory = {};
    var urlBase = 'http://localhost:8080';

    factory.getRole = getRole;

    function getRole(){
        return $http({
            url: urlBase + "/role",
            method: "GET"
        })
    }

    return factory;

}]);