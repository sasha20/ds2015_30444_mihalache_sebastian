/**
 * Created by sasha.mihalache on 12/28/2015.
 */
'use strict';

App.factory('GuitarRepo', ['$http', '$q', function($http, $q){

    var factory = {};
    var urlBase = 'http://localhost:8080';

    factory.get = get;
    factory.getAll = getAll;
    factory.deleteGuitar = deleteGuitar;


    function get(id){
        return $http({
            method: "GET",
            url: urlBase + "/admin/guitar-management/" +id,
            dataType: 'json',
            headers: {
                "Content-Type": "application/json"
            }
        })
    }

    function getAll() {
        return $http({
            method: "GET",
            url: urlBase + "/admin/guitar-management"
        })
    }

    function deleteGuitar(id){
        return $http({
            method: "DELETE",
            url: urlBase + "/admin/guitar-management/" + id,
        })
    }

    return factory;

}]);