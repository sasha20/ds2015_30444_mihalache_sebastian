/**
 * Created by sasha.mihalache on 12/6/2015.
 */

var imageUpload = document.getElementById("imageUpload");
var picture = document.getElementById("picture");
var hiddenImage = document.getElementById("hiddenImage");

var priorValue = picture.src;

imageUpload.onchange = function(){
    var fullPath = imageUpload.value;

    var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
    var filename = fullPath.substring(startIndex);
    if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
        filename = filename.substring(1);
    }

    if(!filename) {
        picture.setAttribute("src", priorValue);
        hiddenImage.setAttribute("value",priorValue);
    }
    else {
        var finalPath = "/resources/img/" + filename;
        picture.setAttribute("src", finalPath);
        hiddenImage.setAttribute("value", finalPath);
        priorValue = finalPath;
    }
};