/**
 * Created by sasha.mihalache on 12/5/2015.
 */

function audioPlayer(attachTo) {
    var player = document.createElement("audio");
    player.controls= true;
    player.id = 'audio';
    var container = document.getElementById(attachTo);

    var stream ="data:audio/mp3;base64,";
    container.appendChild(player);

    this.loadSample = function(sample){

        player.src = stream + sample;

    };

    this.play = function() {
        player.play();
    };

    var loaded = false;

    audio.addEventListener('loadeddata', function(){
        loaded = true;
        //audio.play();
    },false);

    audio.addEventListener('error' , function()
    {
        alert('error loading audio');
    }, false);
}