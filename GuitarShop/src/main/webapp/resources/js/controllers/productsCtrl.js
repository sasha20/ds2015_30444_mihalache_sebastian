/**
 * Created by sasha.mihalache on 12/29/2015.
 */
'use strict';
App.controller('ProductController', ProductController);

ProductController.$injector = ['$scope', 'ProductRepo'];

function ProductController( $scope, ProductRepo ) {
    var vm = this;

    init();

    function init(){
        getAllProducts();

    }



    function getAllProducts(){
        ProductRepo.getAllProducts()
            .then(function(resp) {
                vm.guitars = resp.data;
            })
            .catch(function (err) {
                console.log(err);
            })
    }

}