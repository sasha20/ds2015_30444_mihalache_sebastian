/**
 * Created by sasha.mihalache on 12/28/2015.
 */
'use strict';
App.controller('GuitarController', GuitarController);

GuitarController.$injector = ['$scope', 'GuitarRepo'];

function GuitarController( $scope, GuitarRepo ) {
    var vm = this;

    init();

    function init(){
        getAllGuitars();
    }

    vm.onDeleteGuitarClick = function(id){
        var choice = confirm('Are you sure you want to delete this record?');
        if(choice === true){
            console.log("true");
            deleteGuitar(id);
        }
        else {
        }
    };

    function getGuitar(id){
        GuitarRepo.get(id)
            .then(function(resp){
                console.log(resp)
            })
            .catch(function(err){
                console.log(err);
            });
    }

    function getAllGuitars(){
        GuitarRepo.getAll()
            .then(function(resp) {
                vm.guitars = resp.data;
            })
            .catch(function (err) {
                console.log(err);
            })
    }

    function deleteGuitar(id) {
        GuitarRepo.deleteGuitar(id)
            .then(function(resp){
                console.log(resp);
                getAllGuitars();
                vm.guitars.splice(id,1);
            })
            .catch(function(err){
                console.log(err);
            })
    }
}