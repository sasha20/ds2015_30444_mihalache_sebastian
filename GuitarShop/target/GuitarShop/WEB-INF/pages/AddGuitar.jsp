<%--
  Created by IntelliJ IDEA.
  User: sasha.mihalache
  Date: 12/6/2015
  Time: 9:04 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="for" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
  <title>Guitar Add</title>
  <link   href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css"
          rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
  <link rel="stylesheet" href="/resources/css/styles.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
</head>
<body>
<c:url value="/j_spring_security_logout" var="logoutUrl" />


<nav role="navigation" class="navbar navbar-inverse">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a href="#" class="navbar-brand">Guitar Shop</a>
    </div>
    <!-- Collection of nav links and other content for toggling -->
    <div id="navbarCollapse" class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
            <li><a href="/admin#/user-management" ng-show="vm.role == 'Admin'">Users</a></li>
            <li><a class="active" href="/admin#/guitar-management" ng-show="vm.role =='Admin'">Guitars</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li><a class="nav navbar-brand" href="${logoutUrl}">Logout</a></li>
        </ul>
    </div>
</nav>


<div class="row">

  <div class="col-md-4 col-md-offset-4 panel panel-default">
      <h2 class="panel-heading">Add A Guitar</h2>
      <br/>
      <div class="form-container">
        <form method="post" action="add" class="form-horizontal">

          <input type="hidden"   name="guitar_id"  class="form-control input-lg"/>

          <div class="form-group">
            <label for="brand" class="control-label col-sm-2">Brand:</label>
            <div class="col-sm-9">
                <input type="text" name="brand" class="form-control" id="brand"/>
            </div>
          </div>

          <div class="form-group">
          <label for="name" class="control-label col-sm-2">Name:</label>
            <div class="col-sm-9">
                <input type="text" name="name" class="form-control" id="name"/>
            </div>
          </div>

          <div class="form-group">
              <label for="type" class="control-label col-sm-2">Type:</label>
              <div class="col-sm-9">
                <input type="text" name="type" class="form-control" id="type">
              </div>
          </div>

          <div class="form-group">
              <label for="price" class="control-label col-sm-2">Price: </label>
              <div class="col-sm-9">
                  <input type="number" name="price" class="form-control" id="price">
              </div>
          </div>

          <div class="form-group">
              <label for="color" class="control-label col-sm-2">Color: </label>
              <div class="col-sm-9">
                  <input type="text" name="color"  class="form-control" id="color">
              </div>
          </div>

          <label for="picture">Image:</label> <br/>
          <input type="hidden" name="hiddenImage" id="hiddenImage" class="control-label" />
          <img id="picture" class="img-responsive center-block" src="${guitar.image.toString()}" width="250" height="100" />

          <input type="file" id="imageUpload"/>
            <br/>
          <label for="audioToUpload" class="control-label">Audio Sample:</label>
            <br/><br/>
          <input type="file" id="audioUpload" onchange="sampleUploader(this)"/>

          <div id="audioContainer">
            <input type="hidden"  name="audioToUpload"  id="audioToUpload">
          </div>
            <br/><br/>
          <div class="form-group">
          <label for="quantity" class="control-label col-sm-2">Quantity:</label>
              <div class="col-sm-9">
                <input type="number" name="quantity" class="form-control" id="quantity" />

              </div>
          </div>


          <input type="submit" class="btn btn-info float-right" value="Add New..."/>
            <br /><br/>
        </form>
      </div>
  </div>
</div>

<a href="/admin#/guitar-management">Back</a>

<script src="/resources/js/audioplayer.js"></script>
<script src="/resources/js/SampleUploader.js"></script>
<script src="/resources/js/ImageUploader.js"></script>
</body>

<script>
  <%--var audioPlayer = new audioPlayer("audioContainer");--%>
  <%--audioPlayer.loadSample('${sampleSound}');--%>
</script>
</html>
