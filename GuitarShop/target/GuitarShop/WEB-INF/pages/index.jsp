<%--
  Created by IntelliJ IDEA.
  User: sasha.mihalache
  Date: 12/27/2015
  Time: 6:55 PM
  To change this template use File | Settings | File Templates.
--%>
<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<html lang="en" ng-app="GuitarShop">
<head>
  <meta charset="UTF-8">
  <title>GUITAR SHOP</title>
  <link   href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css"
          rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
  <link rel="stylesheet" href="/resources/css/styles.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
</head>
<body ng-controller="NavbarController as vm">
<c:url value="/j_spring_security_logout" var="logoutUrl" />
<nav role="navigation" class="navbar navbar-inverse">
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
    <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a href="#" class="navbar-brand">Guitar Shop</a>
  </div>
  <!-- Collection of nav links and other content for toggling -->
  <div id="navbarCollapse" class="collapse navbar-collapse">
    <ul class="nav navbar-nav">
      <li><a href="#/user-management" ng-show="vm.role == 'Admin'">Users</a></li>
      <li><a href="#/guitar-management" ng-show="vm.role =='Admin'">Guitars</a></li>
      <li><a href="#/products" ng-show="vm.role == 'User'">Products</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a class="nav navbar-brand" href="${logoutUrl}">Logout</a></li>
    </ul>
  </div>
</nav>
<h3 class="font-fam">Hi {{vm.role}}, you're now logged in</h3>
<br />
<br />


<div class="container">
  <div ng-view=""></div>
</div>



<!-- LIBS -->
<script src="/resources/assets/js/angular.min.js"></script>
<script src="/resources/assets/js/angular-route.min.js"></script>

<!-- APP -->
<script src="/resources/js/config/app.js"></script>
<script src="/resources/js/config/config.js"></script>

<%--Users Module--%>
<script src="/resources/js/services/user.service.js"></script>
<script src="/resources/js/controllers/usersCtrl.js"></script>

<%-- Guitar Module--%>
<script src="/resources/js/services/guitar.service.js"></script>
<script src="/resources/js/controllers/guitarsCtrl.js"></script>

<%--Role Module--%>
<script src="/resources/js/services/role.service.js"></script>

<%--Product Module--%>
<script src="/resources/js/services/product.service.js"></script>
<script src="/resources/js/controllers/productsCtrl.js"></script>
<%--utilitaries--%>

</body>
</html>