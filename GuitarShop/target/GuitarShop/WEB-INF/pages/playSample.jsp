<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: sasha.mihalache
  Date: 12/1/2015
  Time: 5:56 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Playing</title>
  <link   href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css"
          rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
  <link rel="stylesheet" href="/resources/css/styles.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
</head>

<c:url value="/j_spring_security_logout" var="logoutUrl" />




<nav role="navigation" class="navbar navbar-inverse">
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
    <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a href="#" class="navbar-brand">Guitar Shop</a>
  </div>
  <!-- Collection of nav links and other content for toggling -->
  <div id="navbarCollapse" class="collapse navbar-collapse">
    <ul class="nav navbar-nav">
      <li><a href="#/user-management" ng-show="vm.role == 'Admin'">Users</a></li>
      <li><a href="#/guitar-management" ng-show="vm.role =='Admin'">Guitars</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a class="nav navbar-brand" href="${logoutUrl}">Logout</a></li>
    </ul>
  </div>
</nav>

<div class="row">
  <div class="col-md-4 col-md-offset-4 panel panel-default">
    <h2 class="panel-heading">${guitar.brand} ${guitar.name}'s Sound Sample</h2>
    <br/>

    <img  id="image" src="${guitar.image}" width="300" height="200" class="img-responsive"/>
    <br/><br/>
    <div id="audioContainer">

    </div>
    <br />
    <a href="/admin#/guitar-management" class="btn btn-default float-right">Back</a>
    <br/><br/>
  </div>
</div>

<script src="/resources/js/audioplayer.js"></script>
</body>

<script>
  var audioPlayer = new audioPlayer("audioContainer");
  audioPlayer.loadSample('${sampleSound}');
</script>

</html>
