<%--
  Created by IntelliJ IDEA.
  User: sasha.mihalache
  Date: 11/21/2015
  Time: 12:16 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>User Edit</title>
</head>
<body>
<c:url value="/j_spring_security_logout" var="logoutUrl" />
<h2>${user.firstname}'s Edit</h2>

<h3><a href="${logoutUrl}">Logout</a></h3>

<form:form method="post" action="edit" commandName="user">
    <table>
        <tr>
            <td>
                <label for="username">Username: </label>
                <form:input  path="username" />
            </td>
        </tr>
        <tr>
            <td>
                <label for="password">Password: </label>
                <form:input path="password" />
            </td>
        </tr>
        <tr>
            <td>
                <label for="firstname">First Name: </label>
                <form:input path="firstname" />
            </td>
        </tr>
        <tr>
            <td>
                <label for="lastname">Last Name: </label>
                <form:input path="lastname" />
            </td>
        </tr>
        <tr>
            <td>
                <label for="email">Email: </label>
                <form:input path="email" />
            </td>
        </tr>
        <tr>
            <td>
                <label for="telephone">Telephone:</label>
                <form:input path="telephone" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="submit" value="Edit"/>
            </td>
        </tr>
    </table>
</form:form>

<a href="/admin">Back</a>

</body>
</html>
