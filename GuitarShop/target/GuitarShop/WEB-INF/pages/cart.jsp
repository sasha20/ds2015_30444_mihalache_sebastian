<%--
  Created by IntelliJ IDEA.
  User: sasha.mihalache
  Date: 12/27/2015
  Time: 3:08 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Cart Info</title>

    <link   href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css"
            rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="/resources/css/styles.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
</head>

<nav role="navigation" class="navbar navbar-inverse">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a href="#" class="navbar-brand">Guitar Shop</a>
    </div>
    <!-- Collection of nav links and other content for toggling -->
    <div id="navbarCollapse" class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
            <li><a href="/user#/products" ng-show="vm.role == 'User'">Products</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li><a class="nav navbar-brand" href="${logoutUrl}">Logout</a></li>
        </ul>
    </div>
</nav>


<body>

    <div class="row">
        <div class="col-md-4 col-md-offset-4 panel panel-default">
            <h2 class="panel-heading">You Shopping Cart</h2>
            <table  class="table-bordered table-striped">
                <thead>
                  <tr >
                    <th class="correction-padding">Option</th>
                    <th class="correction-padding">Id</th>
                    <th class="correction-padding">Brand</th>
                    <th class="correction-padding">Name</th>
                    <th class="correction-padding">Type</th>
                    <th class="correction-padding">Color</th>
                    <th class="correction-padding">Price</th>
                    <th class="correction-padding">Quantity</th>
                    <th class="correction-padding-subtotal">Subtotal</th>
                  </tr>
                </thead>
              <c:set var="totalSum" value="0"></c:set>
              <c:forEach var="it" items="${sessionScope.cart}">
                  <c:set var="totalSum" value="${ totalSum + it.guitar.price * it.quantity}"></c:set>
                  <tbody>
                  <tr>
                      <td align="center"><a href="/shoppingcart/remove/${it.guitar.guitar_id}">Remove</a></td>
                      <td align="center">${it.guitar.guitar_id}</td>
                      <td align="center">${it.guitar.brand}</td>
                      <td align="center">${it.guitar.name}</td>
                      <td align="center">${it.guitar.type}</td>
                      <td align="center">${it.guitar.color}</td>
                      <td align="center">${it.guitar.price} RON</td>
                      <td align="center">${it.quantity}</td>
                      <td align="center">${it.guitar.price * it.quantity} RON</td>
                  </tr>
              </c:forEach>
                <tr>
                    <td colspan="8" align="right" ><strong>Total</strong></td>
                    <td>${totalSum} RON</td>
                </tr>
                  </tbody>
            </table>
        <br />
            <a class="btn btn-warning float-right" href="/shoppingcart/place-order">Place Order</a>
            <a class="btn btn-info " href="/user#/products">Back to Shopping</a>
            <br/> <br/>
        </div>
    </div>
</body>
</html>
