<%--
  Created by IntelliJ IDEA.
  User: sasha.mihalache
  Date: 12/6/2015
  Time: 3:40 PM
  To change this template use File | Settings | File Templates.
--%>
<%--
  Created by IntelliJ IDEA.
  User: sasha.mihalache
  Date: 11/21/2015
  Time: 12:16 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="for" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Guitar Edit</title>
    <link   href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css"
            rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="/resources/css/styles.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
</head>
<c:url value="/j_spring_security_logout" var="logoutUrl" />

<nav role="navigation" class="navbar navbar-inverse">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a href="#" class="navbar-brand">Guitar Shop</a>
    </div>
    <!-- Collection of nav links and other content for toggling -->
    <div id="navbarCollapse" class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
            <li><a href="#/user-management" ng-show="vm.role == 'Admin'">Users</a></li>
            <li><a href="#/guitar-management" ng-show="vm.role =='Admin'">Guitars</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li><a class="nav navbar-brand" href="${logoutUrl}">Logout</a></li>
        </ul>
    </div>
</nav>

<div class="row">

    <div class="col-md-4 col-md-offset-4 panel panel-default">
        <h2 class="panel-heading">${guitar.brand}'s Edit Page</h2>
        <br/>

        <form method="post" action="/admin/guitar-management/edit"  class="form-horizontal">

            <input type="hidden"   name="guitar_id" value="${guitar.guitar_id}" />

            <div class="form-group">
                <label for="brand" class="control-label col-sm-2">Brand:</label>
                <div class="col-sm-9">
            <input type="text" name="brand" value="${guitar.brand}" id="brand" class="form-control"/>
                </div>
            </div>

            <div class="form-group">
                <label for="name" class="control-label col-sm-2">Name:</label>
                <div class="col-sm-9">
                    <input type="text" name="name" value="${guitar.name}" id="name" class="form-control"/>
                </div>
            </div>

            <div class="form-group">
                <label for="type" class="control-label col-sm-2">Type:</label>
                <div class="col-sm-9">
                    <input type="text" name="type" value="${guitar.type}" id="type" class="form-control">
                </div>
            </div>

            <div class="form-group">
                <label for="price" class="control-label col-sm-2">Price: </label>
                <div class="col-sm-9">
                    <input type="number" name="price" value="${guitar.price}" id="price" class="form-control">
                </div>
            </div>

            <div class="form-group">
                <label for="color" class="control-label col-sm-2">Color: </label>
                <div class="col-sm-9">
                    <input type="text" name="color" value="${guitar.color}" id="color" class="form-control">
                </div>
            </div>

            <label for="hiddenImage">Image:</label> <br/>
            <input type="hidden" name="hiddenImage" id="hiddenImage" value="${guitar.image}" />
            <img id="picture" src="${guitar.image.toString()}" width="250" height="100" class="img-responsive center-block"/>
            <br /><br />
            <input type="file" id="imageUpload"/>
            <br /><br />

            <div class="form-group">
                <label for="quantity" class="control-label col-sm-2">Quantity:</label>
                <div class="col-sm-9">
                    <input type="number" name="quantity" value="${guitar.quantity}" id="quantity" class="form-control"/>
                </div>
            </div>

            <br /><br />
            <input type="file" id="audioUpload" onchange="sampleUploader(this)" />

            <br /><br />
            <div id="audioContainer">

                <input type="hidden"  name="audioToUpload" value="${guitar.sample}" id="audioToUpload">
            </div>

            <br /><br />
            <input class="btn btn-info float-right" type="submit" value="Edit"/>
            <a class="btn btn- float-right" href="/admin#/guitar-management">Back</a>
            <br /><br/>

        </form>
    </div>
</div>

<a href="/admin#/guitar-management">Back</a>

<script src="/resources/js/audioplayer.js"></script>
<script src="/resources/js/SampleUploader.js"></script>
<script src="/resources/js/ImageUploader.js"></script>
</body>

<script>
    var audioPlayer = new audioPlayer("audioContainer");
    audioPlayer.loadSample('${sampleSound}');
</script>
</html>
