<%--
  Created by IntelliJ IDEA.
  User: sasha.mihalache
  Date: 11/15/2015
  Time: 10:08 PM
  To change this template use File | Settings | File Templates.
--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
	<title>Admin Page</title>
</head>
<body>



<c:url value="/j_spring_security_logout" var="logoutUrl" />

	<h2>Admin | You are now logged in</h2>
	<h3><a href="${logoutUrl}">Logout</a></h3>

<h1>User Management Screen </h1>

<h2>Add a user</h2>
<a href="/admin/addUser">Add User</a>

<h3>Users</h3>
<c:if  test="${!empty userList}">
  <table class="data">
    <tr>
      <th>Username</th>
      <th>Password</th>
      <th>Name</th>
      <th>Email</th>
      <th>Telephone</th>
      <th>Action</th>
    </tr>
    <c:forEach items="${userList}" var="users">
      <tr>
        <td>${users.username}</td>
        <td>${users.password}</td>
        <td>${users.firstname} ${users.lastname} </td>
        <td>${users.email}</td>
        <td>${users.telephone}</td>
        <td>
          <a href="admin/editUser/${users.username}">Edit</a>
          <a href="admin/deleteUser/${users.username}">Delete</a>
        </td>
      </tr>
    </c:forEach>
  </table>
</c:if>





<h1>Guitar Management Screen </h1>

<h2>Add a guitar</h2>
<a href="/admin/addGuitar/">Add</a>


<h3>Guitars</h3>
<c:if  test="${!empty guitarList}">
  <table class="data">
    <tr>
      <th>Image</th>
      <th>Brand</th>
      <th>Name</th>
      <th>Type</th>
      <th>Price</th>
      <th>Color</th>
      <th>Sound Sample</th>
      <th>Quantity</th>
      <th>Action</th>
    </tr>
    <c:forEach items="${guitarList}" var="guitars">
      <tr>
        <td><img src="${guitars.image.toString()}" height="200" width="350" /></td>
        <td>${guitars.brand}</td>
        <td>${guitars.name}</td>
        <td>${guitars.type} </td>
        <td>${guitars.price}</td>
        <td>${guitars.color}</td>
        <td><a href="/admin/play/${guitars.guitar_id}">Play</a></td>
        <td>${guitars.quantity}</td>
        <td>
          <a href="admin/editGuitar/${guitars.guitar_id}">Edit</a>
          <a href="admin/deleteGuitar/${guitars.guitar_id}">Delete</a>
        </td>
      </tr>
    </c:forEach>
  </table>
</c:if>

</body>


</html>  