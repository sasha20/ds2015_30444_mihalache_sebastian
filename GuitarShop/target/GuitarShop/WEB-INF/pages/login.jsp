<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
<head>
  <title>Login Form</title>
  <link rel="stylesheet" href="/resources/css/login.css">
</head>
<body>



<div id="login" >
  <form method="post" modelAttribute="loginForm" class="align-form-center" action="<c:url value='/j_spring_security_check' />">
    <input type="text" placeholder="Username..." name="username">
    <input type="password" placeholder="Password..." name="password">
    <input type="submit" value="Login" />
  </form>
  <h5  align="center">${message}</h5>
</div>



<%--<div style="text-align: center; padding: 30px;border: 1px solid green;width: 250px;">--%>
  <%--<form method="post" modelAttribute="loginForm" action="<c:url value='/j_spring_security_check' />">--%>

    <%--<table>--%>
      <%--<tr>--%>
        <%--<td colspan="2" style="color: red">${message}</td>--%>

      <%--</tr>--%>
      <%--<tr>--%>
        <%--<td>User Name:</td>--%>
        <%--<td>--%>
            <%--<input type="text" name="username" />--%>
        <%--</td>--%>
      <%--</tr>--%>
      <%--<tr>--%>
        <%--<td>Password:</td>--%>
        <%--<td><input  type="password" name="password" />--%>
        <%--</td>--%>
      <%--</tr>--%>
      <%--<tr>--%>
        <%--<td> </td>--%>
        <%--<td><input type="submit" value="Login" />--%>
        <%--</td>--%>

      <%--</tr>--%>
    <%--</table>--%>
  <%--</form>--%>
<%--</div>--%>

</body>
</html>  