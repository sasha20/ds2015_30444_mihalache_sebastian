/**
 * Created by sasha.mihalache on 12/27/2015.
 */
'use strict';
App.controller('UserController', UserController);

UserController.$injector = ['$scope', 'UserRepo'];

function UserController( $scope, UserRepo ){
    var vm = this;
    vm.hello = "Hi fag";
    vm.user={username:'',password:'',email:'', firstname:'', lastname:'',telephone:'' };
    vm.found= {username:'',password:'',email:'', firstname:'', lastname:'',telephone:''};
    vm.errors= [];

    init();

    function init(){
        getUsers();
        //createUser(data);
    }

    vm.onResetClick = function() {
        vm.user.username = "";
        vm.user.password = "";
        vm.user.firstname = "";
        vm.user.lastname = "";
        vm.user.email = "";
        vm.user.telephone = "";
        $scope.myForm.$setPristine();
        vm.update = false;
    };

    vm.submit = function() {
        vm.errors = [];
        var data = {
            username: vm.user.username,
            password: vm.user.password,
            firstname: vm.user.firstname,
            lastname: vm.user.lastname,
            email: vm.user.email,
            telephone: vm.user.telephone
        }
       if(vm.update){
           console.log("updated.");
           editUser(data.username, data);
       } else{
           console.log("saved.");
           createUser(data);
       }
    };

    vm.onEditClick = function(username) {
        var ok = getUser(username);
        if(ok) console.log("it was ok");
    };

    vm.onDeleteUserClick = function(username){
        console.log(username);
        var choice = confirm('Are you sure you want to delete this record?');
        if(choice === true){
            console.log("true");
            deleteUser(username);
        }
        else {
        }
    };


    function getUser(username){
        UserRepo.get(username)
            .then(function(response){
                vm.found = response.data;
                vm.update = true;
                vm.user.username = vm.found.username;
                vm.user.password = vm.found.password;
                vm.user.email = vm.found.email;
                vm.user.lastname = vm.found.lastname;
                vm.user.firstname = vm.found.firstname;
                vm.user.telephone = vm.found.telephone;

            })
            .catch(function(err) {
                console.log(err);

            });
        return true;
    }

    function getUsers(){
        UserRepo.getAll()
            .then(function(response) {
                vm.users = response.data;
            })
            .catch(function(err){
                console.log(err);
            });
    }


    function createUser(data){
        UserRepo.create(data)
            .then(function(data){
                getUsers();
            })
            .catch(function(err){
                console.log(err);
                vm.errors.push(err);
            });

    }

    function editUser(id, data){
        UserRepo.editUser(id,data)
            .then(function(data){
                getUsers();
            })
            .catch(function(err) {
                console.log(err);
            });
    }

    function deleteUser(username) {
        UserRepo.deleteUser(username)
            .then(function(data) {

                getUsers();
                vm.users.splice(username,1);
            })
            .catch(function(err) {
                console.log(err);
            });
    }
}