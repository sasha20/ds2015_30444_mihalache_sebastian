/**
 * Created by sasha.mihalache on 12/27/2015.
 */
'use strict';

App.factory('UserRepo', ['$http', '$q', function($http, $q){

    var factory = {};
    var urlBase = 'http://localhost:8080';

    factory.get = get;
    factory.getAll = getAll;
    factory.create = create;
    factory.editUser = editUser;
    factory.deleteUser = deleteUser;

    function get(id) {
        return $http({
            method: 'GET',
            url: urlBase + "/admin/user-management/" + id
        })
    }

    function getAll() {
        return $http({
            method: 'GET',
            url: urlBase + "/admin/user-management",
            dataType: 'json',
            headers: {
                "Content-Type": "application/json"
            }})
    }

    function create(data) {
        return $http({
            method: 'POST',
            url: urlBase + "/admin/user-management",
            data: data,
            headers: {
                "Content-Type": "application/json"
            }
        })
    }

    function editUser(id,data) {
        return $http({
            method: "PUT",
            url: urlBase + "/admin/user-management/" + id,
            data: data
        })
    }

    function deleteUser(id) {
        return $http({
            method: "DELETE",
            url: urlBase + "/admin/user-management/" + id
        })
    }

    return factory;

}]);