/**
 * Created by sasha.mihalache on 12/29/2015.
 */


/**
 * Created by sasha.mihalache on 12/28/2015.
 */
'use strict';

App.factory('ProductRepo', ['$http', '$q', function($http, $q){

    var factory = {};
    var urlBase = 'http://localhost:8080';

    factory.get = get;
    factory.getAllProducts = getAllProducts;


    function get(id){
        return $http({
            method: "GET",
            url: urlBase + "/user/guitar-management/" +id,
            dataType: 'json',
            headers: {
                "Content-Type": "application/json"
            }
        })
    }


    function getAllProducts() {
        return $http({
            method: "GET",
            url: urlBase + "/user/guitar-management"
        })
    }


    return factory;

}]);



