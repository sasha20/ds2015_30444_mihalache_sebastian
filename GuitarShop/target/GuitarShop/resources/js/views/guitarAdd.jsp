<%--
  Created by IntelliJ IDEA.
  User: sasha.mihalache
  Date: 12/6/2015
  Time: 9:04 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="for" uri="http://www.springframework.org/tags/form" %>

<div class="row">

  <div class="col-md-4 col-md-offset-4 panel panel-default">
      <h2 class="panel-heading">Add A Guitar</h2>
      <br/>
      <div class="form-container">
        <form method="post" action="/admin/guitar-management/add" class="form-horizontal">

          <input type="hidden"   name="guitar_id"  class="form-control input-lg"/>

          <div class="form-group">
            <label for="brand" class="control-label col-sm-2">Brand:</label>
            <div class="col-sm-9">
                <input type="text" name="brand" class="form-control" id="brand"/>
            </div>
          </div>

          <div class="form-group">
          <label for="name" class="control-label col-sm-2">Name:</label>
            <div class="col-sm-9">
                <input type="text" name="name" class="form-control" id="name"/>
            </div>
          </div>

          <div class="form-group">
              <label for="type" class="control-label col-sm-2">Type:</label>
              <div class="col-sm-9">
                <input type="text" name="type" class="form-control" id="type">
              </div>
          </div>

          <div class="form-group">
              <label for="price" class="control-label col-sm-2">Price: </label>
              <div class="col-sm-9">
                  <input type="number" name="price" class="form-control" id="price">
              </div>
          </div>

          <div class="form-group">
              <label for="color" class="control-label col-sm-2">Color: </label>
              <div class="col-sm-9">
                  <input type="text" name="color"  class="form-control" id="color">
              </div>
          </div>

          <label for="picture">Image:</label> <br/>
          <input type="hidden" name="hiddenImage" id="hiddenImage" class="control-label" />
          <img id="picture" class="img-responsive center-block" src="${guitar.image.toString()}" width="250" height="100" />

          <input type="file" id="imageUpload"/>
            <br/>
          <label for="audioToUpload" class="control-label">Audio Sample:</label>
            <br/><br/>
          <input type="file" id="audioUpload" onchange="sampleUploader(this)"/>

          <div id="audioContainer">
              <br/>
            <input type="hidden"  name="audioToUpload"  id="audioToUpload">
          </div>
            <br/><br/>
          <div class="form-group">
          <label for="quantity" class="control-label col-sm-2">Quantity:</label>
              <div class="col-sm-9">
                <input type="number" name="quantity" class="form-control" id="quantity" />

              </div>
          </div>


          <input type="submit" class="btn btn-info float-right" value="Add New..."/>
            <a class="btn btn- float-right" href="/admin#/guitar-management">Back</a>
            <br /><br/>
        </form>
      </div>
  </div>

<script src="/resources/js/audioplayer.js"></script>
<script src="/resources/js/SampleUploader.js"></script>
<script src="/resources/js/ImageUploader.js"></script>


<%--<script>--%>
  <%--var audioPlayer = new audioPlayer("audioContainer");--%>
  <%--audioPlayer.loadSample('${sampleSound}');--%>
<%--</script>--%>
</div>