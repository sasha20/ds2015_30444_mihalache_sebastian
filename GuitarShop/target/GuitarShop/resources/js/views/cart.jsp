<%--
  Created by IntelliJ IDEA.
  User: sasha.mihalache
  Date: 12/29/2015
  Time: 6:30 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="container">
  <div class="row">
    <table cellpadding="2" cellspacing="2" border="1">
      <tr>
        <th>Option</th>
        <th>Id</th>
        <th>Brand</th>
        <th>Name</th>
        <th>Type</th>
        <th>Color</th>
        <th>Price</th>
        <th>Quantity</th>
        <th>Subtotal</th>
      </tr>
      <c:set var="totalSum" value="0"></c:set>
      <c:forEach var="it" items="${sessionScope.cart}">
        <c:set var="totalSum" value="${ totalSum + it.guitar.price * it.quantity}"></c:set>
        <tr>
          <td align="center"><a href="/shoppingcart/remove/${it.guitar.guitar_id}">Remove</a></td>
          <td>${it.guitar.guitar_id}</td>
          <td>${it.guitar.brand}</td>
          <td>${it.guitar.name}</td>
          <td>${it.guitar.type}</td>
          <td>${it.guitar.color}</td>
          <td>${it.guitar.price} RON</td>
          <td>${it.quantity}</td>
          <td>${it.guitar.price * it.quantity} RON</td>
        </tr>
      </c:forEach>
      <tr>
        <td colspan="8" align="right">Total</td>
        <td>${totalSum}</td>
      </tr>
    </table>
    <br />
    <a href="/user">Continue Shopping</a>
    <a href="/shoppingcart/place-order">Place Order</a>
  </div>
</div>
