<%--
  Created by IntelliJ IDEA.
  User: sasha.mihalache
  Date: 12/6/2015
  Time: 3:40 PM
  To change this template use File | Settings | File Templates.
--%>
<%--
  Created by IntelliJ IDEA.
  User: sasha.mihalache
  Date: 11/21/2015
  Time: 12:16 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="for" uri="http://www.springframework.org/tags/form" %>

<c:url value="/j_spring_security_logout" var="logoutUrl" />

<div class="row">
    <div class="col-md-4 col-md-offset-4 panel panel-default">
        <h2 class="panel-heading">Edit Guitar</h2>
        <br/>
        <div class="form-container">

        <form method="post" action="/admin/guitar-management/edit" >

            <input type="hidden"   name="guitar_id" value="${guitar.guitar_id}" />

            <label for="brand">Brand:</label>
            <input type="text" name="brand" value="${guitar.brand}" id="brand"/>
            <br/><br/>

            <label for="name">Name:</label>
            <input type="text" name="name" value="${guitar.name}" id="name"/>
            <br /><br />

            <label for="type">Type:</label>
            <input type="text" name="type" value="${guitar.type}" id="type">
            <br /><br />

            <label for="price">Price: </label>
            <input type="number" name="price" value="${guitar.price}" id="price">
            <br /><br />

            <label for="color">Color: </label>
            <input type="text" name="color" value="${guitar.color}" id="color">
            <br /><br />

            <label for="hiddenImage">Image:</label> <br/>
            <input type="hidden" name="hiddenImage" id="hiddenImage" value="${guitar.image}" />
            <img id="picture" src="${guitar.image.toString()}" width="350" height="200" />
            <br /><br />
            <input type="file" id="imageUpload"/>
            <br /><br />

            <label for="quantity">Quantity:</label>
            <input type="number" name="quantity" value="${guitar.quantity}" id="quantity" />


            <br /><br />
            <input type="file" id="audioUpload" onchange="sampleUploader(this)" />

            <br /><br />
            <div id="audioContainer">

                <input type="hidden"  name="audioToUpload" value="${guitar.sample}" id="audioToUpload">
            </div>

            <br /><br />
            <input type="submit" value="Edit"/>

        </form>
        </div>
    </div>

<a href="/admin#/guitar-management">Back</a>

<script src="/resources/js/audioplayer.js"></script>
<script src="/resources/js/SampleUploader.js"></script>
<script src="/resources/js/ImageUploader.js"></script>
<script>
    var audioPlayer = new audioPlayer("audioContainer");
    audioPlayer.loadSample('${sampleSound}');
</script>

</div>