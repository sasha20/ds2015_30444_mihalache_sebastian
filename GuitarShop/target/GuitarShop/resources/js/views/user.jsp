<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: sasha.mihalache
  Date: 12/27/2015
  Time: 7:38 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="container">

    <div class="row">
        <div class="col-md-4">
            <h2>Add a user</h2>
            <span ng-show="vm.errors.length > 0">
                <div ng-repeat="error in vm.errors" class="error-text"> {{error.data.data}}</div>
            </span>
            <form name="myForm" ng-submit="vm.submit()">
                <fieldset class="form-group">
                    <label for="username">Username</label>
                    <input type="text" class="form-control username" id="username" placeholder="Username..." ng-model="vm.user.username"
                           required ng-minlength="3"  name="username" >
                    <div class="has-error" ng-show="myForm.$dirty">
                        <span ng-show="myForm.username.$error.required">This is a required field</span>
                        <span ng-show="myForm.username.$error.minlength">Minimum length required is 3</span>
                        <span ng-show="myForm.username.$invalid">This field is invalid </span>
                    </div>
                </fieldset>
                <fieldset class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control password" id="password" placeholder="Password" ng-model="vm.user.password"
                            required ng-minlength="3" name="password">
                    <div class="has-error" ng-show="myForm.$dirty">
                        <span ng-show="myForm.password.$error.required">This is a required field</span>
                        <span ng-show="myForm.password.$error.minlength">Minimum length required is 3</span>
                        <span ng-show="myForm.password.$invalid">This field is invalid </span>
                    </div>
                </fieldset>
                <fieldset class="form-group">
                    <label for="firstname">First Name</label>
                    <input type="text" class="form-control firstname" id="firstname" placeholder="First Name..." ng-model="vm.user.firstname" name="firstname" required>
                    <div class="has-error" ng-show="myForm.$dirty">
                        <span ng-show="myForm.$dirty && myForm.firstname.$error.required">This is a required field</span>
                    </div>
                </fieldset>
                <fieldset class="form-group">
                    <label for="lastname">Last Name</label>
                    <input type="text" class="form-control lastname" id="lastname" placeholder="Last Name..." ng-model="vm.user.lastname" name="lastname" required>
                    <div class="has-error" ng-show="myForm.$dirty">
                        <span ng-show="myForm.$dirty && myForm.lastname.$error.required">This is a required field</span>
                    </div>
                </fieldset>
                <fieldset class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control email" id="email" placeholder="Email..." ng-model="vm.user.email" name="email" required>
                    <div class="has-error" ng-show="myForm.$dirty">
                        <span ng-show="myForm.$dirty && myForm.email.$error.required">This is a required field</span>
                        <span ng-show="myForm.$dirty && myForm.email.$invalid">This field is invalid </span>
                    </div>
                </fieldset>
                <fieldset class="form-group">
                    <label for="telephone">Phone nr.</label>
                    <input type="tel" class="form-control telephone" id="telephone" placeholder="Phone nr..." ng-model="vm.user.telephone" name="telephone"
                           ng-minlength="10" ng-maxlength="10" required>
                    <div class="has-error" ng-show="myForm.$dirty">
                        <span ng-show="myForm.$dirty && myForm.telephone.$error.required">This is a required field</span>
                        <span ng-show="myForm.telephone.$error.minlength">Minimum length required is 10</span>
                        <span ng-show="myForm.telephone.$error.maxlength">Max length is 10</span>
                    </div>
                </fieldset>
                <div class="form-actions float-right">
                    <input type="submit"  value="{{!vm.update ? 'Add' : 'Update'}}" class="btn btn-primary btn-md" ng-disabled="myForm.$invalid">
                    <button type="button" class="btn btn-warning" ng-click="vm.onResetClick()" ng-disabled="myForm.$pristine && !vm.update">Reset</button>
                </div>
            </form>

        </div>

        <div class="col-md-8">
            <h2>List of Users</h2>

            <table class="table table-striped">
                <tr>
                    <th>Username</th>
                    <th>Password</th>
                    <th>Full Name</th>
                    <th>Email</th>
                    <th>Telephone</th>
                    <th>Action</th>
                </tr>
                <tbody>
                <tr ng-repeat="user in vm.users">
                    <td>{{user.username}}</td>
                    <td>{{user.password}}</td>
                    <td>{{user.firstname}} {{user.lastname}}</td>
                    <td>{{user.email}}</td>
                    <td>{{user.telephone}}</td>
                    <td>
                        <button type="button" class="btn btn-info btn-sm" ng-click="vm.onEditClick(user.username)">Edit</button>
                        <button type="button" class="btn btn-danger btn-sm" ng-click="vm.onDeleteUserClick(user.username)">Delete</button>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

</div>