<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: sasha.mihalache
  Date: 12/1/2015
  Time: 5:56 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="container">


  <a href="/admin#/guitar-management">Back</a>
  <c:url value="/j_spring_security_logout" var="logoutUrl" />
  <h2>${guitar.brand} ${guitar.name}'s Sound Sample</h2>


  <img  id="image" src="${guitar.image}" width="300" height="200"/>

  <br/><br/>

  <div id="audioContainer">
  </div>


  <script src="/resources/js/audioplayer.js"></script>


  <script>
    var audioPlayer = new audioPlayer("audioContainer");
    audioPlayer.loadSample('${sampleSound}');
  </script>

</div>
