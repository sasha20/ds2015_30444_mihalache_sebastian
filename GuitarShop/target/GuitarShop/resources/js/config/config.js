/**
 * Created by sasha.mihalache on 12/27/2015.
 */
(function() {
    'use strict';

    var configModule = angular.module('configModule', []);

    configModule.constant('config', {
        API_URL : 'http://localhost:8080/'
    });

    configModule.config(function($routeProvider) {
        $routeProvider
            .when('/user-management', {
                templateUrl: "resources/js/views/user.jsp",
                controller: 'UserController',
                controllerAs: "vm"
            })
            .when('/guitar-management', {
                templateUrl: "resources/js/views/guitar.jsp",
                controller: "GuitarController",
                controllerAs: "vm"
            })
            .when('/guitar-management/add', {
                templateUrl: "resources/js/views/guitarAdd.jsp",
                //controller: "GuitarAddController",
                //controllerAs: "vm"
            })
            .when('/guitar-management/edit/:guitar_id', {
                templateUrl: "resources/js/views/guitarEdit.jsp"
            })
            .when('/guitar-management/play', {
                templateUrl: "resources/js/views/playGuitarSample.js"
            })
            //.otherwise({
            //    redirectTo: '/'
            //})
            .when('/products', {
                templateUrl: "resources/js/views/products.jsp",
                controller: "ProductController",
                controllerAs: "vm"
        })
    })

})();