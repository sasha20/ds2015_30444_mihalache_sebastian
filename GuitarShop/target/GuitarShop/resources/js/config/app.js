/**
 * Created by sasha.mihalache on 12/27/2015.
 */
'use strict';

var App = angular.module('GuitarShop',[
    'ngRoute',
    'configModule',
]);

App.controller("NavbarController", NavbarController);

NavbarController.$injector = ['RoleRepo'];

function NavbarController(RoleRepo) {
    var vm = this;
    vm.role = "";
    var roleList =["Admin", "User"];
    init();

    function init(){
        getRole();
    }

    RoleRepo.getRole()
        .then(function(resp){

            if(resp.data.data === roleList[0]){
                vm.role = "Admin";
            }
            else if(resp.data.data === roleList[1]){
                vm.role = "User";
            }

        })
        .catch(function(err){
            console.log(err);
        });
    function getRole(){
    }
}