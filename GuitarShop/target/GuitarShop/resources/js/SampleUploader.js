/**
 * Created by sasha.mihalache on 12/6/2015.
 */
function sampleUploader(obj) {
    var sound = document.getElementById('audio');
    var reader = new FileReader();
    var audioToSend = document.getElementById("audioToUpload");

    reader.onload = (function(audio)
    {
        return function(e) {
            if(audio==null){
                audioToSend.value = e.target.result;
            }else{
                audio.src = e.target.result;
    //                var blob = dataURItoBlob(e.target.result);//
                audioToSend.value = e.target.result;

            }
        };
    })(sound);

    reader.addEventListener('load', function() {
        if(sound!=null){
            document.getElementById("audio").play()
        }
    });
    reader.readAsDataURL(obj.files[0]);
}